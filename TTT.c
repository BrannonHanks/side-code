#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/*
	A TIC TAC TOE game, with option to play against the program.
	
	A small project done when first learning C. Coded every possible move, so impossible to beat, but can draw. Could go back and add true AI that learns
	and knows the rules without automatically knowing it's next move, but I might just leave this in the vault for a rainy day.
*/

static char a = '1', b = '2', c = '3', d = '4', e = '5', f = '6', g = '7', h = '8', i = '9';
static int first_move, second_move, third_move, rem_move, rem_move2, rem_move3;

void sleep();

/*return 0 if valid 1 if not*/
static int check_validity (int move) {

  if (move == 1) {
    if (a == '1') {

      return 0;
    } else {

      return 1;
    }
  } else if (move == 2) {
    if (b == '2') {

      return 0;
    } else {

      return 1;
    }
  } else if (move == 3) {
    if (c == '3') {

      return 0;
    } else {

      return 1;
    }
  } else if (move == 4) {
    if (d == '4') {

      return 0;
    } else {

      return 1;
    }
  } else if (move == 5) {
    if (e == '5') {

      return 0;
    } else {

      return 1;
    }
  } else if (move == 6) {
    if (f == '6') {

      return 0;
    } else {

      return 1;
    }
  } else if (move == 7) {
    if (g == '7') {

      return 0;
    } else {

      return 1;
    }
  } else if (move == 8) {
    if (h == '8') {

      return 0;
    } else {

      return 1;
    }
  } else if (move == 9) {
    if (i == '9') {

      return 0;
    } else {

      return 1;
    }
  }
  
  return 1;
}

/*returns 1 if victory 0 if not*/
static int check_victory_p1 (int move) {

  int victory = 0;

  if (move == 1) {
    if (b == 'X' && c == 'X') {
      
      victory = 1;
    }
    if (d == 'X' && g == 'X') {
      
      victory = 1;
    }
    if (e == 'X' && i == 'X') {
    
      victory = 1;
    }
  } else if (move == 2) {
    if (a == 'X' && c == 'X') {
    
      victory = 1;
    }
    if (e == 'X' && h == 'X') {
    
      victory = 1;
    } 
  } else if (move == 3) {
    if (a == 'X' && b == 'X') {
    
      victory = 1;
    }
    if (f == 'X' && i == 'X') {
    
      victory = 1;
    }
    if (e == 'X' && g == 'X') {
    
      victory = 1;
    }
  } else if (move == 4) {
    if (a == 'X' && g == 'X') {
    
      victory = 1;
    }
    if (e == 'X' && f == 'X') {
    
      victory = 1;
    } 
  } else if (move == 5) {
    if (a == 'X' && i == 'X') {
    
      victory = 1;
    } 
    if (b == 'X' && h == 'X') {
      
      victory = 1;
    } 
    if (c == 'X' && g == 'X') {
      
      victory = 1;
    }
    if (d == 'X' && f == 'X') {
      
      victory = 1;
    }
  } else if (move == 6) {
    if (c == 'X' && i == 'X') {
    
      victory = 1;
    }
    if (d == 'X' && e == 'X') {
    
      victory = 1;
    }
  } else if (move == 7) {
    if (a == 'X' && d == 'X') {
    
      victory = 1;
    }
    if (e == 'X' && c == 'X') {
    
      victory = 1;
    }
    if (h == 'X' && i == 'X') {

      victory = 1;
    }
  } else if (move == 8) {
    if (g == 'X' && i == 'X') {
    
      victory = 1;
    }
    if (b == 'X' && e == 'X') {
    
      victory = 1;
    }
  } else if (move == 9) {
    if (a == 'X' && e == 'X') {
    
      victory = 1;
    }
    if (c == 'X' && f == 'X') {
    
      victory = 1;
    }
    if (g == 'X' && h == 'X') {
    
      victory = 1;
    }
  }

  return victory;
}

static int check_victory_p2 (int move) {

  int victory = 0;

  if (move == 1) {
    if (b == 'O' && c == 'O') {
      
      victory = 1;
    }
    if (d == 'O' && g == 'O') {
      
      victory = 1;
    }
    if (e == 'O' && i == 'O') {
    
      victory = 1;
    }
  } else if (move == 2) {
    if (a == 'O' && c == 'O') {
    
      victory = 1;
    }
    if (e == 'O' && h == 'O') {
    
      victory = 1;
    } 
  } else if (move == 3) {
    if (a == 'O' && b == 'O') {
    
      victory = 1;
    }
    if (f == 'O' && i == 'O') {
    
      victory = 1;
    }
    if (e == 'O' && g == 'O') {
    
      victory = 1;
    }
  } else if (move == 4) {
    if (a == 'O' && g == 'O') {
    
      victory = 1;
    }
    if (e == 'O' && f == 'O') {
    
      victory = 1;
    } 
  } else if (move == 5) {
    if (a == 'O' && i == 'O') {
    
      victory = 1;
    } 
    if (b == 'O' && h == 'O') {
      
      victory = 1;
    } 
    if (c == 'O' && g == 'O') {
      
      victory = 1;
    }
    if (d == 'O' && f == 'O') {
      
      victory = 1;
    }
  } else if (move == 6) {
    if (c == 'O' && i == 'O') {
    
      victory = 1;
    }
    if (d == 'O' && e == 'O') {
    
      victory = 1;
    }
  } else if (move == 7) {
    if (a == 'O' && d == 'O') {
    
      victory = 1;
    }
    if (e == 'O' && c == 'O') {
    
      victory = 1;
    }
    if (h == 'O' && i == 'O') {

      victory = 1;
    }
  } else if (move == 8) {
    if (g == 'O' && i == 'O') {
    
      victory = 1;
    }
    if (b == 'O' && e == 'O') {
    
      victory = 1;
    }
  } else if (move == 9) {
    if (a == 'O' && e == 'O') {
    
      victory = 1;
    }
    if (c == 'O' && f == 'O') {
    
      victory = 1;
    }
    if (g == 'O' && h == 'O') {
    
      victory = 1;
    }
  }

  return victory;
}

static void make_move_p1 (int move) {

  if (move == 1) {

    a = 'X';
  } else if (move == 2) {

    b = 'X';
  } else if (move == 3) {

    c = 'X';
  } else if (move == 4) {

    d = 'X';
  } else if (move == 5) {

    e = 'X';
  } else if (move == 6) {

    f = 'X';
  } else if (move == 7) {

    g = 'X';
  } else if (move == 8) {

    h = 'X';
  } else {

    i = 'X';
  }
}

static void make_move_p2 (int move) {

  if (move == 1) {

    a = 'O';
  } else if (move == 2) {

    b = 'O';
  } else if (move == 3) {

    c = 'O';
  } else if (move == 4) {

    d = 'O';
  } else if (move == 5) {

    e = 'O';
  } else if (move == 6) {

    f = 'O';
  } else if (move == 7) {

    g = 'O';
  } else if (move == 8) {

    h = 'O';
  } else {

    i = 'O';
  }
}

static int p3_move (int turn) {

  int move = 0;

  if (turn == 1) {
    if (e == '5') {

      move = 5;
      first_move = 5;

      if (a == 'X') {

    rem_move = 1;
      } else if (b == 'X') {

    rem_move = 2;
      } else if (c == 'X') {

    rem_move = 3;
      } else if (d == 'X') {

    rem_move = 4;
      } else if (f == 'X') {

    rem_move = 6;
      } else if (g == 'X') {

    rem_move = 7;
      } else if (h == 'X') {

    rem_move = 8;
      } else {

    rem_move = 9;
      }
    } else if (a == '1') {

      move = 1;
      first_move = 1;
      rem_move = 5;
    }
  } else if (turn == 2) {

    if (first_move == 5) {

      if (rem_move == 1) {
    if (b == 'X') {

      move = 3;
      second_move = 3;
      rem_move2 = 2;
    } else if (c == 'X') {

      move = 2;
      second_move = 2;
      rem_move2 = 3;
    } else if (d == 'X') {

      move = 7;
      second_move = 7;
      rem_move2 = 4;
    } else if (f == 'X') {

      move = 3;
      second_move = 3;
      rem_move2 = 6;
    } else if (g == 'X') {

      move = 4;
      second_move = 4;
      rem_move2 = 7;
    } else if (h == 'X') {

      move = 7;
      second_move = 7;
      rem_move2 = 8;
    } else {

      move = 8;
      second_move = 8;
      rem_move2 = 9;
    }
      } else if (rem_move == 2) {
    if (a == 'X') {

      move = 3;
      second_move = 3;
      rem_move2 = 1;
    } else if (c == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 3;
    } else if (d == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 4;
    } else if (f == 'X') {

      move = 3;
      second_move = 3;
      rem_move2 = 6;
    } else if (g == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 7;
    } else if (h == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 8;
    } else {

      move = 6;
      second_move = 6;
      rem_move2 = 9;
    }    
      } else if (rem_move == 3) {
    if (b == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 2;
    } else if (a == 'X') {

      move = 2;
      second_move = 2;
      rem_move2 = 1;
    } else if (d == 'X') {

      move = 2;
      second_move = 2;
      rem_move2 = 4;
    } else if (f == 'X') {

      move = 9;
      second_move = 9;
      rem_move2 = 6;
    } else if (g == 'X') {

      move = 2;
      second_move = 2;
      rem_move2 = 7;
    } else if (h == 'X') {

      move = 6;
      second_move = 6;
      rem_move2 = 8;
    } else {

      move = 6;
      second_move = 6;
      rem_move2 = 9;
    }
      } else if (rem_move == 4) {
    if (b == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 2;
    } else if (c == 'X') {

      move = 2;
      second_move = 2;
      rem_move2 = 3;
    } else if (a == 'X') {

      move = 7;
      second_move = 7;
      rem_move2 = 1;
    } else if (f == 'X') {

      move = 3;
      second_move = 3;
      rem_move2 = 2;
    } else if (g == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 7;
    } else if (h == 'X') {

      move = 7;
      second_move = 7;
      rem_move2 = 8;
    } else {

      move = 7;
      second_move = 7;
      rem_move2 = 9;
    }
      } else if (rem_move == 6) {
    if (b == 'X') {

      move = 3;
      second_move = 3;
      rem_move2 = 2;
    } else if (c == 'X') {

      move = 9;
      second_move = 9;
      rem_move2 = 3;
    } else if (d == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 4;
    } else if (a == 'X') {

      move = 2;
      second_move = 2;
      rem_move2 = 3;
    } else if (g == 'X') {

      move = 8;
      second_move = 8;
      rem_move2 = 7;
    } else if (h == 'X') {

      move = 9;
      second_move = 9;
      rem_move2 = 8;
    } else {

      move = 3;
      second_move = 3;
      rem_move2 = 9;
    }
      } else if (rem_move == 7) {
    if (b == 'X') {

      move = 4;
      second_move = 4;
      rem_move2 = 2;
    } else if (c == 'X') {

      move = 2;
      second_move = 2;
      rem_move2 = 3;
    } else if (d == 'X') {

      move = 1;
      second_move = 1;
      rem_move2 = 4;
    } else if (f == 'X') {

      move = 8;
      second_move = 8;
      rem_move2 = 6;
    } else if (a == 'X') {

      move = 4;
      second_move = 4;
      rem_move2 = 1;
    } else if (h == 'X') {

      move = 9;
      second_move = 9;
      rem_move2 = 8;
    } else {

      move = 8;
      second_move = 8;
      rem_move2 = 9;
    }    
      } else if (rem_move == 8) {
    if (b == 'X') {

      move = 4;
      second_move = 4;
      rem_move2 = 2;
    } else if (c == 'X') {

      move = 6;
      second_move = 6;
      rem_move2 = 3;
    } else if (d == 'X') {

      move = 7;
      second_move = 7;
      rem_move2 = 4;
    } else if (f == 'X') {

      move = 9;
      second_move = 9;
      rem_move2 = 6;
    } else if (g == 'X') {

      move = 9;
      second_move = 9;
      rem_move2 = 7;
    } else if (a == 'X') {

      move = 4;
      second_move = 4;
      rem_move2 = 1;
    } else {

      move = 7;
      second_move = 7;
      rem_move2 = 9;
    }
      } else {
    if (b == 'X') {

      move = 6;
      second_move = 6;
      rem_move2 = 2;
    } else if (c == 'X') {

      move = 6;
      second_move = 6;
      rem_move2 = 3;
    } else if (d == 'X') {

      move = 7;
      second_move = 7;
      rem_move2 = 4;
    } else if (f == 'X') {

      move = 3;
      second_move = 3;
      rem_move2 = 6;
    } else if (g == 'X') {

      move = 8;
      second_move = 8;
      rem_move2 = 7;
    } else if (h == 'X') {

      move = 7;
      second_move = 7;
      rem_move2 = 8;
    } else {

      move = 4;
      second_move = 4;
      rem_move2 = 1;
    }
      }
    } else if (first_move == 1) {
      
      if (b == 'X') {

    move = 8;
    second_move = 8;
    rem_move2 = 2;
      } else if (c == 'X') {
    
    move = 7;
    second_move = 7;
    rem_move2 = 3;
      } else if (d == 'X') {

    move = 6;
    second_move = 6;
    rem_move2 = 4;
      } else if (f == 'X') {
    
    move = 4;
    second_move = 4;
    rem_move2 = 6;
      } else if (g == 'X') {

    move = 3;
    second_move = 3;
    rem_move2 = 7;
      } else if (h == 'X') {
    
    move = 2;
    second_move = 2;
    rem_move2 = 8;
      } else {
    
    move = 3;
    second_move = 3;
    rem_move2 = 9;
      }
    }
  } else if (turn == 3) {

    if (first_move == 1) {

      if (rem_move2 == 2) {
    if (c == 'X') {

      move = 7;
      third_move = 7;
      rem_move3 = 3;      
    } else if (d == 'X') {

      move = 6;
      third_move = 6;
      rem_move3 = 4;
    } else if (f == 'X') {

      move = 4;
      third_move = 4;
      rem_move3 = 6;
    } else if (g == 'X') {

      move = 3;
      third_move = 3;
      rem_move3 = 7;
    } else {

      move = 7;
      third_move = 7;
      rem_move3 = 9;
    }
      } else if (rem_move2 == 3) {
    if (d == 'X' ) {

      move = 6;
      third_move = 6;
      rem_move3 = 4;
    } else {

      move = 4;
    }
      } else if (rem_move2 == 4) {
    if (b == 'X') {

      move = 8;
      third_move = 8;
      rem_move3 = 2;
    } else if (c == 'X') {

      move = 7;
      third_move = 7;
      rem_move3 = 3;
    } else if (g == 'X') {

      move = 3;
      third_move = 3;
      rem_move3 = 7;
    } else if (h == 'X') {

      move = 2;
      third_move = 2;
      rem_move3 = 8;
    } else {

      move = 7;
      third_move = 7;
      rem_move3 = 9;
    }
      } else if (rem_move2 == 6) {
    if (g == 'X') {

      move = 3;
      third_move = 3;
      rem_move3 = 7;
    } else {

      move = 7;
    }
      } else if (rem_move2 == 7) {
    if (b == 'X') {

      move = 8;
      third_move = 8;
      rem_move3 = 2;
    } else {

      move = 2;
    }
      } else if (rem_move2 == 8) {
    if (c == 'X') {

      move = 7;
      third_move = 7;
      rem_move3 = 3;
    } else {

      move = 3;
    }
      } else {
    if (b == 'X') {

      move = 8;
      third_move = 8;
      rem_move3 = 2;
    } else {

      move = 2;
    }
      }
    } else {

      if (rem_move == 1) {

    if (rem_move2 == 2) {

      if (g == 'X') {

        move = 4;
        third_move = 4;
        rem_move3 = 7;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 3) {
      
      if (h == 'X') {
        
        move = 4;
        third_move = 4;
        rem_move3 = 8;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 4) {
      
      if (c == 'X') {
        
        move = 2;
        third_move = 2;
        rem_move3 = 3;
      } else {
        
        move = 3;
      }
    } else if (rem_move2 == 6) {

      if (g == 'X') {
        
        move = 4;
        third_move = 4;
        rem_move3 = 7;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 7) {

      if (f == 'X') {
        
        move = 2;
        third_move = 2;
        rem_move3 = 6;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 8) {
      
      if (c == 'X') {

        move = 2;
        third_move = 2;
        rem_move3 = 3;
      } else {
        
        move = 3;
      }
    } else {

      if (b == 'X') {
        
        move = 3;
        third_move = 3;
        rem_move3 = 2;
      } else {
        
        move = 2;
      }
    }
      } else if (rem_move == 2) {

    if (rem_move2 == 1) {

      if (g == 'X') {

        move = 4;
        third_move = 4;
        rem_move3 = 7;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 3) {

      if (i == 'X') {

        move = 6;
        third_move = 6;
        rem_move3 = 9;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 4) {

      if (i == 'X') {

        move = 7;
        third_move = 7;
        rem_move3 = 9;
      } else {
        
        move = 9;
      }
    } else if (rem_move2 == 6) {

      if (g == 'X') {

        move = 9;
        third_move = 9;
        rem_move3 = 7;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 7) {

      if (i == 'X') {

        move = 8;
        third_move = 8;
        rem_move3 = 9;
      } else {
        
        move = 9;
      }
    } else if (rem_move2 == 8) {

      if (i == 'X') {

        move = 7;
        third_move = 7;
        rem_move3 = 9;
      } else {
        
        move = 9;
      }
    } else {

      if (d == 'X') {

        move = 1;
        third_move = 1;
        rem_move3 = 4;
      } else {
        
        move = 4;
      }
    }
      } else if (rem_move == 3) {

    if (rem_move2 == 1) {

      if (h == 'X') {

        move = 6;
        third_move = 6;
        rem_move3 = 8;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 2) {

      if (i == 'X') {

        move = 6;
        third_move = 6;
        rem_move3 = 9;
      } else {
        
        move = 9;
      }
    } else if (rem_move2 == 4) {

      if (h == 'X') {

        move = 7;
        third_move = 7;
        rem_move3 = 8;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 6) {

      if (a == 'X') {

        move = 2;
        third_move = 2;
        rem_move3 = 1;
      } else {
        
        move = 1;
      }
    } else if (rem_move2 == 7) {

      if (h == 'X') {

        move = 9;
        third_move = 9;
        rem_move3 = 8;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 8) {

      if (d == 'X') {

        move = 7;
        third_move = 7;
        rem_move3 = 4;
      } else {
        
        move = 4;
      }
    } else {

      if (d == 'X') {

        move = 8;
        third_move = 8;
        rem_move3 = 4;
      } else {
        
        move = 4;
      }
    }
      } else if (rem_move == 4) {

    if (rem_move2 == 1) {

      if (c == 'X') {

        move = 2;
        third_move = 2;
        rem_move3 = 3;
      } else {
        
        move = 3;
      }
    } else if (rem_move2 == 2) {

      if (i == 'X') {

        move = 3;
        third_move = 3;
        rem_move3 = 9;
      } else {
        
        move = 9;
      }
    } else if (rem_move2 == 3) {

      if (h == 'X') {

        move = 7;
        third_move = 7;
        rem_move3 = 8;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 6) {

      if (g == 'X') {

        move = 1;
        third_move = 1;
        rem_move3 = 7;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 7) {

      if (i == 'X') {

        move = 8;
        third_move = 8;
        rem_move3 = 9;
      } else {
        
        move = 9;
      }
    } else if (rem_move2 == 8) {

      if (c == 'X') {

        move = 1;
        third_move = 1;
        rem_move3 = 3;
      } else {
        
        move = 3;
      }
    } else {

      if (c == 'X') {

        move = 6;
        third_move = 6;
        rem_move3 = 3;
      } else {
        
        move = 3;
      }
    }
      } else if (rem_move == 6) {

    if (rem_move2 == 1) {

      if (h == 'X') {

        move = 9;
        third_move = 9;
        rem_move3 = 8;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 2) {

      if (g == 'X') {

        move = 1;
        third_move = 1;
        rem_move3 = 7;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 3) {

      if (a == 'X') {

        move = 2;
        third_move = 2;
        rem_move3 = 1;
      } else {
        
        move = 1;
      }
    } else if (rem_move2 == 4) {

      if (i == 'X') {

        move = 3;
        third_move = 3;
        rem_move3 = 9;
      } else {
        
        move = 9;
      }
    } else if (rem_move2 == 7) {

      if (b == 'X') {

        move = 3;
        third_move = 3;
        rem_move3 = 2;
      } else {
        
        move = 2;
      }
    } else if (rem_move2 == 8) {

      if (a == 'X') {

        move = 7;
        third_move = 7;
        rem_move3 = 1;
      } else {
        
        move = 1;
      }
    } else {

      if (g == 'X') {

        move = 8;
        third_move = 8;
        rem_move3 = 7;
      } else {
        
        move = 7;
      }
    }
      } else if (rem_move == 7) {

    if (rem_move2 == 1) {

      if (f == 'X') {

        move = 8;
        third_move = 8;
        rem_move3 = 6;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 2) {

      if (f == 'X') {

        move = 3;
        third_move = 3;
        rem_move3 = 6;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 3) {

      if (h == 'X') {

        move = 9;
        third_move = 9;
        rem_move3 = 8;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 4) {

      if (i == 'X') {

        move = 8;
        third_move = 8;
        rem_move3 = 9;
      } else {
        
        move = 9;
      }
    } else if (rem_move2 == 6) {

      if (b == 'X') {

        move = 3;
        third_move = 3;
        rem_move3 = 2;
      } else {
        
        move = 2;
      }
    } else if (rem_move2 == 8) {

      if (a == 'X') {

        move = 4;
        third_move = 4;
        rem_move3 = 1;
      } else {
        
        move = 1;
      }
    } else {

      if (b == 'X') {

        move = 4;
        third_move = 4;
        rem_move3 = 2;
      } else {
        
        move = 2;
      }
    }
      } else if (rem_move == 8) {

    if (rem_move2 == 1) {

      if (f == 'X') {

        move = 9;
        third_move = 9;
        rem_move3 = 6;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 2) {

      if (f == 'X') {

        move = 1;
        third_move = 1;
        rem_move3 = 6;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 3) {

      if (d == 'X') {

        move = 7;
        third_move = 7;
        rem_move3 = 4;
      } else {
        
        move = 4;
      }
    } else if (rem_move2 == 4) {

      if (c == 'X') {

        move = 1;
        third_move = 1;
        rem_move3 = 3;
      } else {
        
        move = 3;
      }
    } else if (rem_move2 == 6) {

      if (a == 'X') {

        move = 3;
        third_move = 3;
        rem_move3 = 1;
      } else {
        
        move = 1;
      }
    } else if (rem_move2 == 7) {

      if (a == 'X') {

        move = 4;
        third_move = 4;
        rem_move3 = 1;
      } else {
        
        move = 1;
      }
    } else {

      if (c == 'X') {

        move = 6;
        third_move = 6;
        rem_move3 = 3;
      } else {
        
        move = 3;
      }
    }
      } else {

    if (rem_move2 == 1) {

      if (f == 'X') {

        move = 3;
        third_move = 3;
        rem_move3 = 6;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 2) {

      if (d == 'X') {

        move = 1;
        third_move = 1;
        rem_move3 = 4;
      } else {
        
        move = 4;
      }
    } else if (rem_move2 == 3) {

      if (d == 'X') {

        move = 2;
        third_move = 2;
        rem_move3 = 4;
      } else {
        
        move = 4;
      }
    } else if (rem_move2 == 4) {

      if (c == 'X') {

        move = 6;
        third_move = 6;
        rem_move3 = 3;
      } else {
        
        move = 3;
      }
    } else if (rem_move2 == 6) {

      if (g == 'X') {

        move = 8;
        third_move = 8;
        rem_move3 = 7;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 7) {

      if (b == 'X') {

        move = 4;
        third_move = 4;
        rem_move3 = 2;
      } else {
        
        move = 2;
      }
    } else {

      if (c == 'X') {

        move = 6;
        third_move = 6;
        rem_move3 = 3;
      } else {

        move = 3;
      }
    }
      }
    }
  } else {

    if (first_move == 1) {

      if (rem_move2 == 2) {

    if (rem_move3 == 3) {

      if (i == 'X') {

        move = 6;
      } else {

        move = 9;
      }
    } else if (rem_move3 == 4) {

      if (c == 'X') {

        move = 7;
      } else {

        move = 3;
      }
    } else if (rem_move3 == 6) {

      if (g == 'X') {

        move = 3;
      } else {

        move = 7;
      }
    } else if (rem_move3 == 7) {

      if (d == 'X') {

        move = 6;
      } else {

        move = 4;
      }
    } else {

      if (d == 'X') {

        move = 6;
      } else {

        move = 4;
      }
    }
      } else if (rem_move2 == 3) {

      if (b == 'X') {

        move = 8;
      } else {

        move = 2;
      }
      } else if (rem_move2 == 4) {

    if (rem_move3 == 2) {

      if (c == 'X') {

        move = 7;
      } else {

        move = 3;
      }
    } else if (rem_move3 == 3) {

      if (b == 'X') {

        move = 8;
      } else {

        move = 2;
      }
    } else if (rem_move3 == 7) {

      if (b == 'X') {

        move = 8;
      } else {

        move = 2;
      }
    } else if (rem_move3 == 8) {

      if (c == 'X') {

        move = 7;
      } else {

        move = 3;
      }
    } else {

      if (b == 'X') {

        move = 8;
      } else {

        move = 2;
      }
    }
      } else if (rem_move2 == 6) {

      if (b == 'X') {

        move = 8;
      } else {

        move = 2;
      }
      } else if (rem_move2 == 7) {

      if (d == 'X') {

        move = 6;
      } else {

        move = 4;
      }
      } else if (rem_move2 == 8) {

      if (d == 'X') {

        move = 6;
      } else {

        move = 4;
      }
      } else {

      if (d == 'X') {

        move = 6;
      } else {

        move = 4;
      }
      }
    } else {

      if (rem_move == 1) {

    if (rem_move2 == 2) {

      if (f == 'X') {

        move = 8;
      } else {

        move = 6;
      }
    } else if (rem_move2 == 3) {

      if (f == 'X') {

        move = 9;
      } else {

        move = 6;
      }
    } else if (rem_move2 == 4) {

      if (h == 'X') {

        move = 9;
      } else {

        move = 8;
      }
    } else if (rem_move2 == 6) {

      if (h == 'X') {

        move = 9;
      } else {

        move = 8;
      }
    } else if (rem_move2 == 7) {

      if (h == 'X') {

        move = 9;
      } else {

        move = 8;
      }
    } else if (rem_move2 == 8) {

      if (f == 'X') {

        move = 9;
      } else {

        move = 6;
      }
    } else {

      if (d == 'X') {

        move = 7;
      } else {

        move = 4;
      }
    }
      } else if (rem_move == 2) {

    if (rem_move2 == 1) {

      if (f == 'X') {

        move = 9;
      } else {

        move = 6;
      }
    } else if (rem_move2 == 3) {

      if (d == 'X') {

        move = 7;
      } else {

        move = 4;
      }
    } else if (rem_move2 == 4) {

      if (f == 'X') {

        move = 3;
      } else {

        move = 6;
      }
    } else if (rem_move2 == 6) {

      if (a == 'X') {

        move = 4;
      } else {

        move = 1;
      }
    } else if (rem_move2 == 7) {

      if (f == 'X') {

        move = 3;
      } else {

        move = 6;
      }
    } else if (rem_move2 == 8) {

      if (d == 'X') {

        move = 6;
      } else {

        move = 4;
      }
    } else {

      if (g == 'X') {

        move = 8;
      } else {

        move = 7;
      }
    }
      } else if (rem_move == 3) {

    if (rem_move2 == 1) {

      if (d == 'X') {

        move = 7;
      } else {

        move = 4;
      }
    } else if (rem_move2 == 2) {

      if (d == 'X') {

        move = 7;
      } else {

        move = 4;
      }
    } else if (rem_move2 == 4) {

      if (f == 'X') {

        move = 9;
      } else {

        move = 6;
      }
    } else if (rem_move2 == 6) {

      if (h == 'X') {

        move = 7;
      } else {

        move = 8;
      }
    } else if (rem_move2 == 7) {

      if (a == 'X') {

        move = 4;
      } else {

        move = 1;
      }
    } else if (rem_move2 == 8) {

      if (b == 'X') {

        move = 1;
      } else {

        move = 2;
      }
    } else {

      if (b == 'X') {

        move = 1;
      } else {

        move = 2;
      }
    }
      } else if (rem_move == 4) {

    if (rem_move2 == 1) {

      if (f == 'X') {

        move = 9;
      } else {

        move = 6;
      }
    } else if (rem_move2 == 2) {

      if (g == 'X') {

        move = 8;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 3) {

      if (f == 'X') {

        move = 9;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 6) {

      if (i == 'X') {

        move = 8;
      } else {
        
        move = 9;
      }
    } else if (rem_move2 == 7) {

      if (b == 'X') {

        move = 3;
      } else {
        
        move = 2;
      }
    } else if (rem_move2 == 8) {

      if (i == 'X') {

        move = 6;
      } else {
        
        move = 9;
      }
    } else {

      if (b == 'X') {

        move = 1;
      } else {
        
        move = 2;
      }
    }
      } else if (rem_move == 6) {

    if (rem_move2 == 1) {

      if (d == 'X') {

        move = 7;
      } else {
        
        move = 4;
      }
    } else if (rem_move2 == 2) {

      if (h == 'X') {

        move = 9;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 3) {

      if (g == 'X') {

        move = 4;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 4) {

      if (h == 'X') {

        move = 7;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 7) {

      if (d == 'X') {

        move = 1;
      } else {
        
        move = 4;
      }
    } else if (rem_move2 == 8) {

      if (b == 'X') {

        move = 3;
      } else {
        
        move = 2;
      }
    } else {

      if (a == 'X') {

        move = 4;
      } else {
        
        move = 1;
      }
    }
      } else if (rem_move == 7) {

    if (rem_move2 == 1) {

      if (b == 'X') {

        move = 3;
      } else {
        
        move = 2;
      }
    } else if (rem_move2 == 2) {

      if (h == 'X') {

        move = 9;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 3) {

      if (d == 'X') {

        move = 1;
      } else {
        
        move = 4;
      }
    } else if (rem_move2 == 4) {

      if (f == 'X') {

        move = 3;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 6) {

      if (d == 'X') {

        move = 1;
      } else {
        
        move = 4;
      }
    } else if (rem_move2 == 8) {

      if (b == 'X') {

        move = 3;
      } else {
        
        move = 2;
      }
    } else {

      if (f == 'X') {

        move = 3;
      } else {
        
        move = 6;
      }
    }
      } else if (rem_move == 8) {

    if (rem_move2 == 1) {

      if (b == 'X') {

        move = 3;
      } else {
        
        move = 2;
      }
    } else if (rem_move2 == 2) {

      if (g == 'X') {

        move = 9;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 3) {

      if (a == 'X') {

        move = 2;
      } else {
        
        move = 1;
      }
    } else if (rem_move2 == 4) {

      if (f == 'X') {

        move = 9;
      } else {
        
        move = 6;
      }
    } else if (rem_move2 == 6) {

      if (d == 'X') {

        move = 7;
      } else {
        
        move = 4;
      }
    } else if (rem_move2 == 7) {

      if (f == 'X') {

        move = 3;
      } else {
        
        move = 6;
      }
    } else {

      if (a == 'X') {

        move = 2;
      } else {
        
        move = 1;
      }
    }
      } else {

    if (rem_move2 == 1) {

      if (g == 'X') {

        move = 8;
      } else {
        
        move = 7;
      }
    } else if (rem_move2 == 2) {

      if (h == 'X') {

        move = 7;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 3) {

      if (h == 'X') {

        move = 7;
      } else {
        
        move = 8;
      }
    } else if (rem_move2 == 4) {

      if (a == 'X') {

        move = 2;
      } else {
        
        move = 1;
      }
    } else if (rem_move2 == 6) {

      if (b == 'X') {

        move = 1;
      } else {
        
        move = 2;
      }
    } else if (rem_move2 == 7) {

      if (f == 'X') {

        move = 3;
      } else {
        
        move = 6;
      }
    } else {

      if (d == 'X') {

        move = 1;
      } else {
        
        move = 4;
      }
    }
      }
    }
  }

  sleep(1);
  printf("%d", move);

  return move;
}

static void clear_board() {

  a = '1';
  b = '2';
  c = '3';
  d = '4';
  e = '5';
  f = '6';
  g = '7';
  h = '8';
  i = '9';
}

static void print_board () {

  printf("%c %c %c %c %c\n", a, '|', b, '|', c);
  printf("%c %c %c %c %c\n", '_', '_', '_', '_', '_');
  printf("%c %c %c %c %c\n", d, '|', e, '|', f);
  printf("%c %c %c %c %c\n", '_', '_', '_', '_', '_');
  printf("%c %c %c %c %c\n\n", g, '|', h, '|', i);

}

/*
static void insults () {
have the comp throw taunts. You know you want to.
}
*/

static int get_avialable () {

  int r, check = 1;

  while (check) {

    srand(time(NULL));
    r = rand() % (8);
    r += 1;
    check = check_validity(r);
  }
 
  printf("%d", r);

  return r;
}

/*Be sure to check if no player can win*/
int tictac () {

  int game_over = 0, turn = 0, difficulty, stop  = 0;
  char p1[100];
  char p2[100];
  char p3[8] = {"Eleicia"};
  int game_mode, cur_move, p1_turn = 1, p2_turn = 1;

  system("clear");

  printf("How many players?\n1 or 2: ");
  scanf("%d", &game_mode);

  if (game_mode == 1) {

    printf("Please enter your name p1: ");
    scanf("%s", p1);
    printf("What difficulty would you like?\n1. Easy\n2. Hard\nUser: ");
    scanf("%d", &difficulty);

    if (difficulty != 1 && difficulty != 2) {

      printf("Can't follow simple instructions? This little stunt is going in your file.\nThe future computer overlords will be joyed to learn of this insubordination!\n");

      return 0;
    }

    while (!stop) {

      printf("\n");
      printf("Hello. My name is %s. It seems we are play together. Good Luck %s!\n", p3, p1);
      printf("\n");

      while (!game_over) {

    turn++;
    print_board();

    while (p1_turn) {

      printf("%s it is your turn: ", p1);
      scanf("%d", &cur_move);
      printf("\n");
      p1_turn = check_validity(cur_move);

      if (p1_turn) {
      
        printf("Not a valid move.\n\n");
      } else {
        
        make_move_p1(cur_move);
      }
    }
    p1_turn = 1;

    print_board();
    game_over = check_victory_p1(cur_move);

    if (turn == 5 && !game_over) {

      break;
    }

    if (!game_over) {

      printf("%s it is your turn: ", p3);
    
      if (difficulty == 2) {

        cur_move = p3_move(turn);
        printf("\n\n");
        make_move_p2(cur_move);
        game_over = check_victory_p2(cur_move);
      } else {
        
        cur_move = get_avialable();
        printf("\n\n");
        make_move_p2(cur_move);
        game_over = check_victory_p2(cur_move);
      }

      if (game_over) {

        print_board();
        printf("%s wins!!!\n", p3);
        printf("No offense, but I am a computer. You never stood a chance.\n");
      }
    } else {

      print_board();
      printf("%s wins!!!\n", p1);
      printf("How about you let me win the next one? - XO %s\n", p3);
    }
      }

      printf("\nWould you like to replay?\n   1. Quick rematch\n   2. New game\n   3. No\n\n");
      scanf("%d", &stop);

      if (stop == 2) {

    stop = 0;
    game_over = 0;
    clear_board();
    tictac();
      } else if (stop == 1) {

    stop = 0;
    game_over = 0;
    clear_board();
      }

    }
    return 0;
  } else if (game_mode == 2) {

    printf("Please enter your name p1: ");
    scanf("%s", p1);
    printf("\n");
    printf("Please enter your name p2: ");
    scanf("%s", p2);
    printf("\n");

    while (!stop) {

      turn = 0;
      while (!game_over) {

    turn++;
    print_board();

    while (p1_turn) {

      printf("%s it is your turn: ", p1);
      scanf("%d", &cur_move);
      printf("\n");
      p1_turn = check_validity(cur_move);

      if (p1_turn) {

        printf("Not a valid move.\n\n");
      } else {

        make_move_p1(cur_move);
      }
    }
    p1_turn = 1;
    
    print_board();
    game_over = check_victory_p1(cur_move);

    if (turn == 5 && !game_over) {

      break;
    }

    if (!game_over) {

      while (p2_turn) {

        printf("%s it is your turn: ", p2);
        scanf("%d", &cur_move);
        printf("\n");
        p2_turn = check_validity(cur_move);
        
        if (p2_turn) {
          
          printf("Not a valid move.\n");
        } else {
          
          make_move_p2(cur_move);
        }
      }
      p2_turn = 1;
      
      game_over = check_victory_p2(cur_move);
      
      if (game_over) {
        
        print_board();
        printf("%s wins!!!\n", p2);
      }
    } else {
      
      print_board();
      printf("%s wins!!!\n", p1);
    }
      }
      
      printf("\nWould you like to replay?\n   1. Quick rematch\n   2. New game\n   3. No\n\n");
      scanf("%d", &stop);

      if (stop == 2) {

    stop = 0;
    game_over = 0;
    clear_board();
    tictac();
      } else if (stop == 1) {

    stop = 0;
    game_over = 0;
    clear_board();
      }

    }
    return 0;
  } else {

    printf("Can't follow simple instructions? This little stunt is going in your file.\nThe future computer overlords will be joyed to learn of this insubordination!\n");
    return 0;
  }
}