<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>matchMe</title>
		<link rel="stylesheet" href="mainstyle.css" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	</head>
	
	<body>
		<h1>Send Message</h1>
		
		<?PHP
			#This goes hand in hand with the inbox. This is how messages are sent. Without the conjoining code,from my group, all of the messages 
			#are from alice.
			$to = "";
			if(isset($_POST['specMess'])){
				$to = $_POST['to'];
			}
			if(isset($_POST['mesCon'])){
				echo $_FILES['filename']['type'];
				if ($_FILES['filename']['name'] === ""){
					$messageServer = new userDatabaseServer($_POST['to'], "Alice", $_POST['top'], $_POST['mess'], 
						"NA", "NA");
					$err = $messageServer->dbInsert();
					echo "No Pic";
				} else {
					$messageServer = new userDatabaseServer($_POST['to'], "Alice", $_POST['top'], $_POST['mess'], 
						addslashes(file_get_contents($_FILES['filename']['tmp_name'])), $_FILES['filename']['type']);
					$err = $messageServer->dbInsert();
					echo "<br/>Pic<br/>";
				}
				if ($err === 1) {
					$resp =<<< EOM
						<script>
							alert("Message Failed to Send");
							window.location="messages.php";
						</script>
EOM;
					echo $resp;
				} else {
					$resp =<<< EOM
						<script>
							alert("Message Sent");
							window.location="messages.php";
						</script>
EOM;
					echo $resp;
				}
			}

			$mesForm =<<< EOM
				<form action="sendMessage.php" enctype="multipart/form-data" method="post">
					To: <input type="text" name="to" value="$to">
					<br/>
					Topic: <input type="text" name="top" value=""><br/>
					Message: <br/><textarea rows="5" cols="80" name="mess"></textarea>
					<br/>
					Attach a Picture: <input type="file" name="filename"/>
					<br/>
					<input type="submit" name="mesCon" value="Send">
				</form>
				<br/><br/>
				
EOM;
			echo $mesForm;
		?>
		
		<form action="messages.php" method="post">
			<input type="submit" name="testi" value="Back to Inbox">
		</form>
		<form action="MainPage.php" method="post">
			<input type="submit" name="testi" value="Back to Profile">
		</form>
	</body>
</html>


<?PHP
	class userDatabaseServer {
		private $to;
		private $from;
		private $topic;
		private $message;
		private $picture;
		private $docMime;
		private $messages;
		private $topics;
		private $froms;
		
		public function __construct($uTo, $uFrom, $uTopic, $uMessage, $uPicture, $uDoc) {
			$this->to = $uTo;
			$this->from = $uFrom;
			$this->topic = $uTopic;
			$this->message = $uMessage;
			$this->picture = $uPicture;
			$this->docMime = $uDoc;
			$this->messages = array();
			$this->topics = array();
			$this->froms = array();
			$this->host = "localhost";
			$this->user = "user";
			$this->dbPassword = "passLove";
			$this->database = "matchMe";
			$this->table = "messages";
		}
		
		public function getPic () {
			return $this->picture;
		}
		
		public function getMes ($id) {
			return $this->messages[$id];
		}
		
		public function getTop ($id) {
			return $this->topics[$id];
		}
		
		public function getFrom ($id) {
			return $this->froms[$id];
		}
		
		private function connectToDB($host, $user, $password, $database) {
			$db = mysqli_connect($host, $user, $password, $database);
			if (mysqli_connect_errno()) {
				echo "Connect failed.\n".mysqli_connect_error();
				exit();
			}
			return $db;
		}
		
		public function dbInsert() {
			$db = $this->connectToDB($this->host, $this->user, $this->dbPassword, $this->database);
			$table = $this->table;
			$sqlQuery = "insert into $table (mTo, mFrom, Topic, Message, Picture, docMimeType) values 
				('{$this->to}', '{$this->from}', '{$this->topic}', '{$this->message}', '{$this->picture}', '{$this->docMime}')";
			$result = mysqli_query($db, $sqlQuery);
			if ($result) {
				$err = 0;
			} else { 				   
				$err = 1;
			}

			mysqli_close($db);
			return $err;
		}
	}
?>