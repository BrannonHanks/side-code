<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<title>matchMe</title>
		<link rel="stylesheet" href="mainstyle.css" type="text/css" />
		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
		<script src="//code.jquery.com/jquery-1.10.2.js"></script>
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	</head>
	
	<body>
		<h1>Message Inbox</h1>
		
		<?PHP
			#This is my part of the code for a group project. THis handles the inbox that users get for messages between them.
			#Right now is only works as though the user is Alice, but with the code combined I would take in the user stored in a session.
			$messageCount;
			$messageServer = new userDatabaseServer("", "Alice", "", "", "", "", "");
			if(isset($_POST['del'])){
				$messageServer->delete($_POST['ID']);
			}
			$messageCount = $messageServer->Messages();
			for($i = 0; $i < $messageCount; $i++) {
				$mes = $messageServer->getMes($i);
				$top = $messageServer->getTop($i);
				$fro = $messageServer->getFrom($i);
				$pId = $messageServer->getId($i);
				$doc = $messageServer->getDoc($i);
				if ($doc !== "NA") {
					$messageDoc =<<< EOM
						<div id="m$i" title="$top">$mes<br/><br/><img src="messageImageGet.php?id=$pId" alt="no $pId" height="160" width="160"><br/>From: $fro<br/><br/><form action="sendMessage.php" method="post">
							<input type="hidden" name="to" value="$fro"><input type="submit" name="specMess" value="Reply"></form></div>				
EOM;
				} else {
					$messageDoc =<<< EOM
						<div id="m$i" title="$top">$mes<br/><br/>From: $fro<br/><br/><form action="sendMessage.php" method="post">
							<input type="hidden" name="to" value="$fro"><input type="submit" name="specMess" value="Reply"></form></div>				
EOM;
				}
				echo $messageDoc;
			};
			echo "<h1>$messageCount</h1>";
		?>
		
		<script>
			var foo = 0;
			<?PHP
				for($i = 0; $i < $messageCount; $i++) {
					$popU =<<< EOB
						$("#m$i").dialog({autoOpen:false});
						$("#$i").click(function() {;
						$("#m$i").dialog('open');});
EOB;
					echo $popU;
				}
			?>
		</script>
		
		<form action="MainPage.php" method="post">
			<input type="submit" name="testi" value="Back to Profile">
		</form>
		<form action="sendMessage.php" method="post">
			<input type="submit" name="newMess" value="Compose">
		</form>
	</body>
</html>


<?PHP
	class userDatabaseServer {
		private $to;
		private $from;
		private $topic;
		private $message;
		private $picture;
		private $docMime;
		private $messages;
		private $topics;
		private $froms;
		private $ids;
		private $docs;
		
		public function __construct($uTo, $uFrom, $uTopic, $uMessage, $uPicture, $uDoc) {
			$this->to = $uTo;
			$this->from = $uFrom;
			$this->topic = $uTopic;
			$this->message = $uMessage;
			$this->picture = $uPicture;
			$this->docMime = $uDoc;
			$this->messages = array();
			$this->topics = array();
			$this->froms = array();
			$this->ids = array();
			$this->docs = array();
			$this->host = "localhost";
			$this->user = "user";
			$this->dbPassword = "passLove";
			$this->database = "matchMe";
			$this->table = "messages";
		}
		
		public function getMes ($id) {
			return $this->messages[$id];
		}
		
		public function getTop ($id) {
			return $this->topics[$id];
		}
		
		public function getFrom ($id) {
			return $this->froms[$id];
		}
		
		public function getDoc($id) {
			return $this->docs[$id];
		}
		
		public function getId ($id) {
			return $this->ids[$id];
		}
		
		public function delete($id) {
			$db = $this->connectToDB($this->host, $this->user, $this->dbPassword, $this->database);
			$table = $this->table;
			$sqlQuery = sprintf("delete from %s where id=%d", $table, $id);
			$result = mysqli_query($db, $sqlQuery);
		}
		
		private function connectToDB($host, $user, $password, $database) {
			$db = mysqli_connect($host, $user, $password, $database);
			if (mysqli_connect_errno()) {
				echo "Connect failed.\n".mysqli_connect_error();
				exit();
			}
			return $db;
		}
		
		public function Messages() {
			$id = 0;
			$db = $this->connectToDB($this->host, $this->user, $this->dbPassword, $this->database);
			$table = $this->table;
			$mfrom = mysqli_real_escape_string($db,$this->from);
			$sqlQuery = sprintf("select * from %s", $table);
			$result = mysqli_query($db, $sqlQuery);
			if ($result) {
				$numberOfRows = mysqli_num_rows($result);
				if ($numberOfRows == 0) {
					echo "<h1>You have no mail.</h1>";
				} else {
					echo "<table border='1'>";
					while ($recordArray = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
					echo "<tr><td>{$recordArray['mFrom']}</td><td><a href='#' id='$id'>{$recordArray['Topic']}</a></td><td><form action='messages.php' method='post'><input type='hidden' name='ID' value='{$recordArray['id']}'><input type='submit' name='del' value='Delete'></form></td></tr>";
						$this->messages[$id] = $recordArray['Message'];
						$this->topics[$id] = $recordArray['Topic'];
						$this->froms[$id] = $recordArray['mFrom'];
						$this->ids[$id] = $recordArray['id'];
						$this->docs[$id] = $recordArray['docMimeType'];
						$id++;
					}
					echo "</table>";
				}
				mysqli_free_result($result);
			}  else {
				echo "Retrieving records failed.".mysqli_error($db);
			}

			mysqli_close($db);
			return $id;
		}
	}
?>