import urllib
import re
import time

#a Small Python Program that counts up the number of deceased in obituaries from around the US. If you lose connection the program dies. A catch should be
#add to keep retrying, but I am worried about a lock-up. Topic may be a little morbid, but I feel that this data is readily available and easy to obtain,
#so it worked as a good first basis to gather data on the web in a uniform way.

tick = time.time()
url = "http://www.obituaries.com/ns/obituariescom/obits.aspx"
html = urllib.urlopen(url)
reg = re.compile('NewspaperLink".+"(.{,70})" target')
x = reg.findall(html.read())
deaders = 0
wrongs = []
reg1 = re.compile('([0-9]+) t?T?otal')
reg2 = re.compile('LatestObitItem')
for d in x:
	print d
	html = urllib.urlopen(d)
	hold = html.read()
	y = reg1.search(hold)
	if y:
		deaders = int(y.group(1)) + deaders
		print y.group(1)
	else:
		y1 = reg2.findall(hold)
		if y1:
			print len(y1)
			deaders += len(y1)
		else:
			wrongs += [d]
ticker = time.time()
print "Time taken"
print (ticker-tick)
print "Total Deaders"
print deaders
print "Non-workers"
print len(wrongs)
print len(x)
for d in wrongs:
	print d