#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MAX 80
#define ARTILLERY 5

int sleep();

/*
	A BattleShip game
	
	This was written in about a week and a half. When I was first learning C. I would love to go back and change so much of this and streamline it.
	Cutting the code in half. There is much redundancy, but I want to keep it more as a reminder of the perseverance and willingness to code.
	The random function needs work, if I recall. I screwed something up with the seeding. And easy fix when I go back to this, 'if' I do.
	I added in a new game mode called Hunt For Red October. You place your ships and hunt for the enemy sub. You have 20 turns before you lose.
	Both this and the TicTacToe were designed to be forked from a shell. I think I removed all dependencies on this side, but some code might have slipped in.

	Methods: 	check_squares - 74
				extract - 936
				extract_t - 1044
				check_placement - 1154
				place_square - 2844
				place_piece - 3294
				check_move - 3396
				fire - 4259
				check_victory_comp - 7989
				check_victory - 8031
				check_victory_t - 8103
				random - 8108
				print_board - 8118 
				print_start - 8166
				print_start2 - 8191
				new_board - 8215
				battle - 8243
 */

static int sizet;

typedef struct piece {

  const char * spot;
  int full;

} Piece;

typedef struct board {
  Piece a1, a2, a3, a4, a5, a6, a7, a8, a9, a10,
    b1, b2, b3, b4, b5, b6, b7, b8, b9, b10,
    c1, c2, c3, c4, c5, c6, c7, c8, c9, c10,
    d1, d2, d3, d4, d5, d6, d7, d8, d9, d10,
    e1, e2, e3, e4, e5, e6, e7, e8, e9, e10,
    f1, f2, f3, f4, f5, f6, f7, f8, f9, f10,
    g1, g2, g3, g4, g5, g6, g7, g8, g9, g10,
    h1, h2, h3, h4, h5, h6, h7, h8, h9, h10,
    i1, i2, i3, i4, i5, i6, i7, i8, i9, i10,
    j1, j2, j3, j4, j5, j6, j7, j8, j9, j10,
    pa1, pa2, pa3, pa4, pa5, pa6, pa7, pa8, pa9, pa10,
    pb1, pb2, pb3, pb4, pb5, pb6, pb7, pb8, pb9, pb10,
    pc1, pc2, pc3, pc4, pc5, pc6, pc7, pc8, pc9, pc10,
    pd1, pd2, pd3, pd4, pd5, pd6, pd7, pd8, pd9, pd10,
    pe1, pe2, pe3, pe4, pe5, pe6, pe7, pe8, pe9, pe10,
    pf1, pf2, pf3, pf4, pf5, pf6, pf7, pf8, pf9, pf10,
    pg1, pg2, pg3, pg4, pg5, pg6, pg7, pg8, pg9, pg10,
    ph1, ph2, ph3, ph4, ph5, ph6, ph7, ph8, ph9, ph10,
    pi1, pi2, pi3, pi4, pi5, pi6, pi7, pi8, pi9, pi10,
    pj1, pj2, pj3, pj4, pj5, pj6, pj7, pj8, pj9, pj10;
  int ds, cr, sb, bs, ac;

} Board;

/*Return 1 if good, 0 if bad*/
static int check_squares (Board * b, char move, int num) {

  int check;

  if (move == 'a' || move == 'A') {
    if (num == 1) {
      if (b->pa1.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 2) {
      if (b->pa2.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 3) {
      if (b->pa3.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 4) {
      if (b->pa4.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 5) {
      if (b->pa5.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 6) {
      if (b->pa6.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 7) {
      if (b->pa7.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 8) {
      if (b->pa8.full == 0) {
    
    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pa9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pa10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {
      
      check = 0;
    }
  } else if (move == 'b' || move == 'B') {
    if (num == 1) {
      if (b->pb1.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 2) {
      if (b->pb2.full == 0) {
    
    check = 1;
      } else {
    
    check = 0;
      }
    } else if (num == 3) {
      if (b->pb3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->pb4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->pb5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->pb6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->pb7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->pb8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pb9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pb10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else if (move == 'c' || move == 'C') {
    if (num == 1) {
      if (b->pc1.full == 0) {
    
    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (b->pc2.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (b->pc3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->pc4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->pc5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->pc6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->pc7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->pc8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pc9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pc10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else if (move == 'd' || move == 'D') {
    if (num == 1) {
      if (b->pd1.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (b->pd2.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (b->pd3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->pd4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->pd5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->pd6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->pd7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->pd8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pd9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pd10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else if (move == 'e' || move == 'E') {
    if (num == 1) {
      if (b->pe1.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (b->pe2.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (b->pe3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->pe4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->pe5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->pe6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->pe7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->pe8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pe9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pe10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else if (move == 'f' || move == 'F') {
    if (num == 1) {
      if (b->pf1.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (b->pf2.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (b->pf3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->pf4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->pf5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->pf6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->pf7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->pf8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pf9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pf10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else if (move == 'g' || move == 'G') {
    if (num == 1) {
      if (b->pg1.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (b->pg2.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (b->pg3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->pg4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->pg5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->pg6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->pg7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->pg8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pg9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pg10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else if (move == 'h' || move == 'H') {
    if (num == 1) {
      if (b->ph1.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (b->ph2.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (b->ph3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->ph4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->ph5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->ph6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->ph7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->ph8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->ph9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->ph10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else if (move == 'i' || move == 'I') {
    if (num == 1) {
      if (b->pi1.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (b->pi2.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (b->pi3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->pi4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->pi5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->pi6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->pi7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->pi8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pi9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pi10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else if (move == 'j' || move == 'J') {
    if (num == 1) {
      if (b->pj1.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (b->pj2.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (b->pj3.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (b->pj4.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (b->pj5.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (b->pj6.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (b->pj7.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (b->pj8.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (b->pj9.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (b->pj10.full == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 0;
    }
  } else {

    check = 0;
  }

  return check;
}

static int extract (Board * b, char move, char move2, int first, int second, int size) {

  int p, q, loop, check = 0, good = 0;

  if (first == second) {
    if (move == 'a' || move == 'A') {

      p = 65;
    } else if (move == 'b' || move == 'B') {

      p = 66;
    } else if (move == 'c' || move == 'C') {

      p = 67;
    } else if (move == 'd' || move == 'D') {

      p = 68;
    } else if (move == 'e' || move == 'E') {

      p = 69;
    } else if (move == 'f' || move == 'F') {

      p = 70;
    } else if (move == 'g' || move == 'G') {

      p = 71;
    } else if (move == 'h' || move == 'H') {

      p = 72;
    } else if (move == 'i' || move == 'I') {

      p = 73;
    } else if (move == 'j' || move == 'J') {

      p = 74;
    }

    if (move2 == 'a' || move2 == 'A') {

      q = 65;
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
    }

    if (p < q) {

      for (loop = p; loop <= q; loop++) {

    check += check_squares(b, (char)loop, first);
      }
    } else {

      for (loop = q; loop <= p; loop++) {

    check += check_squares(b, (char)loop, first);
      }
    }
    
  } else {

    if (first < second) {

      for (loop = first; loop <= second; loop++) {

    check += check_squares(b, move, loop);
      }
    } else {

      for (loop = second; loop <= first; loop++) {

    check += check_squares(b, move, loop);
      }
    }
  }

  if (check == size) {

    good = 1;
  }
 
  return good;
}

static void extract_t (char move, char move2, int first, int second, char a[], int b[]) {

  int p, q, loop;

  if (first == second) {
    if (move == 'a' || move == 'A') {

      p = 65;
    } else if (move == 'b' || move == 'B') {

      p = 66;
    } else if (move == 'c' || move == 'C') {

      p = 67;
    } else if (move == 'd' || move == 'D') {

      p = 68;
    } else if (move == 'e' || move == 'E') {

      p = 69;
    } else if (move == 'f' || move == 'F') {

      p = 70;
    } else if (move == 'g' || move == 'G') {

      p = 71;
    } else if (move == 'h' || move == 'H') {

      p = 72;
    } else if (move == 'i' || move == 'I') {

      p = 73;
    } else if (move == 'j' || move == 'J') {

      p = 74;
    }

    if (move2 == 'a' || move2 == 'A') {

      q = 65;
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
    }

    if (p < q) {

      for (loop = p; loop <= q; loop++) {

     a[sizet] = (char)loop;
     b[sizet] = first;
     sizet++;
      }
    } else {

      for (loop = q; loop <= p; loop++) {

    a[sizet] = (char)loop;
    b[sizet] = first;
    sizet++;
      }
    }
    
  } else {

    if (first < second) {

      for (loop = first; loop <= second; loop++) {

     a[sizet] = move;
     b[sizet] = loop;
     sizet++;
      }
    } else {

      for (loop = second; loop <= first; loop++) {

     a[sizet] = move;
     b[sizet] = loop;
     sizet++;
      }
    }
  }
}

/*Returns 0 if good, 1 if bad*/
static int check_placement (Board * b, char move, char move2, int first, int second, int size) {

  int check = 1, movement, p, q;

  if (first < 1 || first > 10) {

    return 1;
  } else if (second < 1 || second > 10) {

    return 1;
  }

  if (move == 'a' || move == 'A') {

    p = 65;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else if (move == 'b' || move == 'B') {

    p = 66;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else if (move == 'c' || move == 'C') {

    p = 67;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else if (move == 'd' || move == 'D') {

    p = 68;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else if (move == 'e' || move == 'E') {

    p = 69;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    }
  } else if (move == 'f' || move == 'F') {

    p = 70;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else if (move == 'g' || move == 'G') {

    p = 71;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else if (move == 'h' || move == 'H') {

    p = 72;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else if (move == 'i' || move == 'I') {

    p = 73;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else if (move == 'j' || move == 'J') {

    p = 74;

    if(move2 == 'a' || move2 == 'A') {

      q = 65;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {
    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1 && first == second) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
      movement = abs((p - q) + (first - second));
      if (movement == size - 1) {

    if (extract(b, move, move2, first, second, size)) {

      check = 0;
    } else {
      check = 1;
    }
      } else {

    check = 1;
      }
    } else {

      check = 1;
    } 
  } else {

    check = 1;
  }

  return check;
}

static void place_square (Board * b, char move, int num, int size) {

  const char * n;
  int m;

  if (size == 2) {

    n = "DS";
    m = 2;
  } else if (size == 3) {

    n = "CR";
    m = 3;
  } else if (size == 4) {

    n = "SB";
    m = 4;
  } else if (size == 5) {

    n = "BS";
    m = 5;
  } else {

    n = "AC";
    m = 6;
  }

  if (move == 'a' || move == 'A') {
    if (num == 1) {

      b->pa1.spot = n;
      b->pa1.full = m;
    } else if (num == 2) {

      b->pa2.spot = n;
      b->pa2.full = m;
    } else if (num == 3) {

      b->pa3.spot = n;
      b->pa3.full = m;
    } else if (num == 4) {

      b->pa4.spot = n;
      b->pa4.full = m;
    } else if (num == 5) {

      b->pa5.spot = n;
      b->pa5.full = m;
    } else if (num == 6) {

      b->pa6.spot = n;
      b->pa6.full = m;
    } else if (num == 7) {

      b->pa7.spot = n;
      b->pa7.full = m;
    } else if (num == 8) {

      b->pa8.spot = n;
      b->pa8.full = m;
    } else if (num == 9) {

      b->pa9.spot = n;
      b->pa9.full = m;
    } else {

      b->pa10.spot = n;
      b->pa10.full = m;
    }
  } else if (move == 'b' || move == 'B') {
    if (num == 1) {

      b->pb1.spot = n;
      b->pb1.full = m;
    } else if (num == 2) {

      b->pb2.spot = n;
      b->pb2.full = m;
    } else if (num == 3) {

      b->pb3.spot = n;
      b->pb3.full = m;
    } else if (num == 4) {

      b->pb4.spot = n;
      b->pb4.full = m;
    } else if (num == 5) {

      b->pb5.spot = n;
      b->pb5.full = m;
    } else if (num == 6) {

      b->pb6.spot = n;
      b->pb6.full = m;
    } else if (num == 7) {

      b->pb7.spot = n;
      b->pb7.full = m;
    } else if (num == 8) {

      b->pb8.spot = n;
      b->pb8.full = m;
    } else if (num == 9) {

      b->pb9.spot = n;
      b->pb9.full = m;
    } else {

      b->pb10.spot = n;
      b->pb10.full = m;
    }
  } else if (move == 'c' || move == 'C') {
    if (num == 1) {

      b->pc1.spot = n;
      b->pc1.full = m;
    } else if (num == 2) {

      b->pc2.spot = n;
      b->pc2.full = m;
    } else if (num == 3) {

      b->pc3.spot = n;
      b->pc3.full = m;
    } else if (num == 4) {

      b->pc4.spot = n;
      b->pc4.full = m;
    } else if (num == 5) {

      b->pc5.spot = n;
      b->pc5.full = m;
    } else if (num == 6) {

      b->pc6.spot = n;
      b->pc6.full = m;
    } else if (num == 7) {

      b->pc7.spot = n;
      b->pc7.full = m;
    } else if (num == 8) {

      b->pc8.spot = n;
      b->pc8.full = m;
    } else if (num == 9) {

      b->pc9.spot = n;
      b->pc9.full = m;
    } else {

      b->pc10.spot = n;
      b->pc10.full = m;
    }
  } else if (move == 'd' || move == 'D') {
    if (num == 1) {

      b->pd1.spot = n;
      b->pd1.full = m;
    } else if (num == 2) {

      b->pd2.spot = n;
      b->pd2.full = m;
    } else if (num == 3) {

      b->pd3.spot = n;
      b->pd3.full = m;
    } else if (num == 4) {

      b->pd4.spot = n;
      b->pd4.full = m;
    } else if (num == 5) {

      b->pd5.spot = n;
      b->pd5.full = m;
    } else if (num == 6) {

      b->pd6.spot = n;
      b->pd6.full = m;
    } else if (num == 7) {

      b->pd7.spot = n;
      b->pd7.full = m;
    } else if (num == 8) {

      b->pd8.spot = n;
      b->pd8.full = m;
    } else if (num == 9) {

      b->pd9.spot = n;
      b->pd9.full = m;
    } else {

      b->pd10.spot = n;
      b->pd10.full = m;
    }
  } else if (move == 'e' || move == 'E') {
    if (num == 1) {

      b->pe1.spot = n;
      b->pe1.full = m;
    } else if (num == 2) {

      b->pe2.spot = n;
      b->pe2.full = m;
    } else if (num == 3) {

      b->pe3.spot = n;
      b->pe3.full = m;
    } else if (num == 4) {

      b->pe4.spot = n;
      b->pe4.full = m;
    } else if (num == 5) {

      b->pe5.spot = n;
      b->pe5.full = m;
    } else if (num == 6) {

      b->pe6.spot = n;
      b->pe6.full = m;
    } else if (num == 7) {

      b->pe7.spot = n;
      b->pe7.full = m;
    } else if (num == 8) {

      b->pe8.spot = n;
      b->pe8.full = m;
    } else if (num == 9) {

      b->pe9.spot = n;
      b->pe9.full = m;
    } else {

      b->pe10.spot = n;
      b->pe10.full = m;
    }
  } else if (move == 'f' || move == 'F') {
    if (num == 1) {

      b->pf1.spot = n;
      b->pf1.full = m;
    } else if (num == 2) {

      b->pf2.spot = n;
      b->pf2.full = m;
    } else if (num == 3) {

      b->pf3.spot = n;
      b->pf3.full = m;
    } else if (num == 4) {

      b->pf4.spot = n;
      b->pf4.full = m;
    } else if (num == 5) {

      b->pf5.spot = n;
      b->pf5.full = m;
    } else if (num == 6) {

      b->pf6.spot = n;
      b->pf6.full = m;
    } else if (num == 7) {

      b->pf7.spot = n;
      b->pf7.full = m;
    } else if (num == 8) {

      b->pf8.spot = n;
      b->pf8.full = m;
    } else if (num == 9) {

      b->pf9.spot = n;
      b->pf9.full = m;
    } else {

      b->pf10.spot = n;
      b->pf10.full = m;
    }
  } else if (move == 'g' || move == 'G') {
    if (num == 1) {

      b->pg1.spot = n;
      b->pg1.full = m;
    } else if (num == 2) {

      b->pg2.spot = n;
      b->pg2.full = m;
    } else if (num == 3) {

      b->pg3.spot = n;
      b->pg3.full = m;
    } else if (num == 4) {

      b->pg4.spot = n;
      b->pg4.full = m;
    } else if (num == 5) {

      b->pg5.spot = n;
      b->pg5.full = m;
    } else if (num == 6) {

      b->pg6.spot = n;
      b->pg6.full = m;
    } else if (num == 7) {

      b->pg7.spot = n;
      b->pg7.full = m;
    } else if (num == 8) {

      b->pg8.spot = n;
      b->pg8.full = m;
    } else if (num == 9) {

      b->pg9.spot = n;
      b->pg9.full = m;
    } else {

      b->pg10.spot = n;
      b->pg10.full = m;
    }
  } else if (move == 'h' || move == 'H') {
    if (num == 1) {

      b->ph1.spot = n;
      b->ph1.full = m;
    } else if (num == 2) {

      b->ph2.spot = n;
      b->ph2.full = m;
    } else if (num == 3) {

      b->ph3.spot = n;
      b->ph3.full = m;
    } else if (num == 4) {

      b->ph4.spot = n;
      b->ph4.full = m;
    } else if (num == 5) {

      b->ph5.spot = n;
      b->ph5.full = m;
    } else if (num == 6) {

      b->ph6.spot = n;
      b->ph6.full = m;
    } else if (num == 7) {

      b->ph7.spot = n;
      b->ph7.full = m;
    } else if (num == 8) {

      b->ph8.spot = n;
      b->ph8.full = m;
    } else if (num == 9) {

      b->ph9.spot = n;
      b->ph9.full = m;
    } else {

      b->ph10.spot = n;
      b->ph10.full = m;
    }
  } else if (move == 'i' || move == 'I') {
    if (num == 1) {

      b->pi1.spot = n;
      b->pi1.full = m;
    } else if (num == 2) {

      b->pi2.spot = n;
      b->pi2.full = m;
    } else if (num == 3) {

      b->pi3.spot = n;
      b->pi3.full = m;
    } else if (num == 4) {

      b->pi4.spot = n;
      b->pi4.full = m;
    } else if (num == 5) {

      b->pi5.spot = n;
      b->pi5.full = m;
    } else if (num == 6) {

      b->pi6.spot = n;
      b->pi6.full = m;
    } else if (num == 7) {

      b->pi7.spot = n;
      b->pi7.full = m;
    } else if (num == 8) {

      b->pi8.spot = n;
      b->pi8.full = m;
    } else if (num == 9) {

      b->pi9.spot = n;
      b->pi9.full = m;
    } else {

      b->pi10.spot = n;
      b->pi10.full = m;
    }
  } else {
    if (num == 1) {

      b->pj1.spot = n;
      b->pj1.full = m;
    } else if (num == 2) {

      b->pj2.spot = n;
      b->pj2.full = m;
    } else if (num == 3) {

      b->pj3.spot = n;
      b->pj3.full = m;
    } else if (num == 4) {

      b->pj4.spot = n;
      b->pj4.full = m;
    } else if (num == 5) {

      b->pj5.spot = n;
      b->pj5.full = m;
    } else if (num == 6) {

      b->pj6.spot = n;
      b->pj6.full = m;
    } else if (num == 7) {

      b->pj7.spot = n;
      b->pj7.full = m;
    } else if (num == 8) {

      b->pj8.spot = n;
      b->pj8.full = m;
    } else if (num == 9) {

      b->pj9.spot = n;
      b->pj9.full = m;
    } else {

      b->pj10.spot = n;
      b->pj10.full = m;
    }
  }
}

static void place_piece (Board * b, char move, char move2, int first, int second, int size) {

  int p, q, loop;

  if (first == second) {
    if (move == 'a' || move == 'A') {

      p = 65;
    } else if (move == 'b' || move == 'B') {

      p = 66;
    } else if (move == 'c' || move == 'C') {

      p = 67;
    } else if (move == 'd' || move == 'D') {

      p = 68;
    } else if (move == 'e' || move == 'E') {

      p = 69;
    } else if (move == 'f' || move == 'F') {

      p = 70;
    } else if (move == 'g' || move == 'G') {

      p = 71;
    } else if (move == 'h' || move == 'H') {

      p = 72;
    } else if (move == 'i' || move == 'I') {

      p = 73;
    } else if (move == 'j' || move == 'J') {

      p = 74;
    }

    if (move2 == 'a' || move2 == 'A') {

      q = 65;
    } else if (move2 == 'b' || move2 == 'B') {

      q = 66;
    } else if (move2 == 'c' || move2 == 'C') {

      q = 67;
    } else if (move2 == 'd' || move2 == 'D') {

      q = 68;
    } else if (move2 == 'e' || move2 == 'E') {

      q = 69;
    } else if (move2 == 'f' || move2 == 'F') {

      q = 70;
    } else if (move2 == 'g' || move2 == 'G') {

      q = 71;
    } else if (move2 == 'h' || move2 == 'H') {

      q = 72;
    } else if (move2 == 'i' || move2 == 'I') {

      q = 73;
    } else if (move2 == 'j' || move2 == 'J') {

      q = 74;
    }

    if (p < q) {

      for (loop = p; loop <= q; loop++) {

    place_square(b, (char)loop, first, size);
      }
    } else {

      for (loop = q; loop <= p; loop++) {

    place_square(b, (char)loop, first, size);
      }
    }
    
  } else {

    if (first < second) {

      for (loop = first; loop <= second; loop++) {

    place_square(b, move, loop, size);
      }
    } else {

      for (loop = second; loop <= first; loop++) {

    place_square(b, move, loop, size);
      }
    }
  }
}

/*returns 0 if good. 1 if used, or 2 if terrible.*/
static int check_move (Board * b, Board * b2, char move, int num) {

  int check;

  if (move == 'a' || move == 'A') {
    if (num == 1) {
      if (strcmp(b->a1.spot, "XX") == 0 || strcmp(b->a1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->a2.spot, "XX") == 0 || strcmp(b->a2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->a3.spot, "XX") == 0 || strcmp(b->a3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->a4.spot, "XX") == 0 || strcmp(b->a4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->a5.spot, "XX") == 0 || strcmp(b->a5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->a6.spot, "XX") == 0 || strcmp(b->a6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->a7.spot, "XX") == 0 || strcmp(b->a7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->a8.spot, "XX") == 0 || strcmp(b->a8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->a9.spot, "XX") == 0 || strcmp(b->a9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->a10.spot, "XX") == 0 || strcmp(b->a10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'b' || move == 'B') {
    if (num == 1) {
      if (strcmp(b->b1.spot, "XX") == 0 || strcmp(b->b1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->b2.spot, "XX") == 0 || strcmp(b->b2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->b3.spot, "XX") == 0 || strcmp(b->b3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->b4.spot, "XX") == 0 || strcmp(b->b4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->b5.spot, "XX") == 0 || strcmp(b->b5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->b6.spot, "XX") == 0 || strcmp(b->b6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->b7.spot, "XX") == 0 || strcmp(b->b7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->b8.spot, "XX") == 0 || strcmp(b->b8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->b9.spot, "XX") == 0 || strcmp(b->b9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->b10.spot, "XX") == 0 || strcmp(b->b10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'c' || move == 'C') {
    if (num == 1) {
      if (strcmp(b->c1.spot, "XX") == 0 || strcmp(b->c1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->c2.spot, "XX") == 0 || strcmp(b->c2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->c3.spot, "XX") == 0 || strcmp(b->c3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->c4.spot, "XX") == 0 || strcmp(b->c4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->c5.spot, "XX") == 0 || strcmp(b->c5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->c6.spot, "XX") == 0 || strcmp(b->c6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->c7.spot, "XX") == 0 || strcmp(b->c7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->c8.spot, "XX") == 0 || strcmp(b->c8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->c9.spot, "XX") == 0 || strcmp(b->c9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->c10.spot, "XX") == 0 || strcmp(b->c10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'd' || move == 'D') {
    if (num == 1) {
      if (strcmp(b->d1.spot, "XX") == 0 || strcmp(b->d1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->d2.spot, "XX") == 0 || strcmp(b->d2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->d3.spot, "XX") == 0 || strcmp(b->d3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->d4.spot, "XX") == 0 || strcmp(b->d4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->d5.spot, "XX") == 0 || strcmp(b->d5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->d6.spot, "XX") == 0 || strcmp(b->d6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->d7.spot, "XX") == 0 || strcmp(b->d7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->d8.spot, "XX") == 0 || strcmp(b->d8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->d9.spot, "XX") == 0 || strcmp(b->d9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->d10.spot, "XX") == 0 || strcmp(b->d10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'e' || move == 'E') {
    if (num == 1) {
      if (strcmp(b->e1.spot, "XX") == 0 || strcmp(b->e1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->e2.spot, "XX") == 0 || strcmp(b->e2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->e3.spot, "XX") == 0 || strcmp(b->e3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->e4.spot, "XX") == 0 || strcmp(b->e4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->e5.spot, "XX") == 0 || strcmp(b->e5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->e6.spot, "XX") == 0 || strcmp(b->e6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->e7.spot, "XX") == 0 || strcmp(b->e7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->e8.spot, "XX") == 0 || strcmp(b->e8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->e9.spot, "XX") == 0 || strcmp(b->e9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->e10.spot, "XX") == 0 || strcmp(b->e10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'f' || move == 'F') {
    if (num == 1) {
      if (strcmp(b->f1.spot, "XX") == 0 || strcmp(b->f1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->f2.spot, "XX") == 0 || strcmp(b->f2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->f3.spot, "XX") == 0 || strcmp(b->f3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->f4.spot, "XX") == 0 || strcmp(b->f4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->f5.spot, "XX") == 0 || strcmp(b->f5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->f6.spot, "XX") == 0 || strcmp(b->f6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->f7.spot, "XX") == 0 || strcmp(b->f7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->f8.spot, "XX") == 0 || strcmp(b->f8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->f9.spot, "XX") == 0 || strcmp(b->f9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->f10.spot, "XX") == 0 || strcmp(b->f10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'g' || move == 'G') {
    if (num == 1) {
      if (strcmp(b->g1.spot, "XX") == 0 || strcmp(b->g1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->g2.spot, "XX") == 0 || strcmp(b->g2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->g3.spot, "XX") == 0 || strcmp(b->g3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->g4.spot, "XX") == 0 || strcmp(b->g4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->g5.spot, "XX") == 0 || strcmp(b->g5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->g6.spot, "XX") == 0 || strcmp(b->g6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->g7.spot, "XX") == 0 || strcmp(b->g7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->g8.spot, "XX") == 0 || strcmp(b->g8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->g9.spot, "XX") == 0 || strcmp(b->g9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->g10.spot, "XX") == 0 || strcmp(b->g10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'h' || move == 'H') {
    if (num == 1) {
      if (strcmp(b->h1.spot, "XX") == 0 || strcmp(b->h1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->h2.spot, "XX") == 0 || strcmp(b->h2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->h3.spot, "XX") == 0 || strcmp(b->h3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->h4.spot, "XX") == 0 || strcmp(b->h4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->h5.spot, "XX") == 0 || strcmp(b->h5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->h6.spot, "XX") == 0 || strcmp(b->h6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->h7.spot, "XX") == 0 || strcmp(b->h7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->h8.spot, "XX") == 0 || strcmp(b->h8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->h9.spot, "XX") == 0 || strcmp(b->h9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->h10.spot, "XX") == 0 || strcmp(b->h10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'i' || move == 'I') {
    if (num == 1) {
      if (strcmp(b->i1.spot, "XX") == 0 || strcmp(b->i1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->i2.spot, "XX") == 0 || strcmp(b->i2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->i3.spot, "XX") == 0 || strcmp(b->i3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->i4.spot, "XX") == 0 || strcmp(b->i4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->i5.spot, "XX") == 0 || strcmp(b->i5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->i6.spot, "XX") == 0 || strcmp(b->i6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->i7.spot, "XX") == 0 || strcmp(b->i7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->i8.spot, "XX") == 0 || strcmp(b->i8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->i9.spot, "XX") == 0 || strcmp(b->i9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->i10.spot, "XX") == 0 || strcmp(b->i10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else if (move == 'j' || move == 'J') {
    if (num == 1) {
      if (strcmp(b->j1.spot, "XX") == 0 || strcmp(b->j1.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 2) {
      if (strcmp(b->j2.spot, "XX") == 0 || strcmp(b->j2.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 3) {
      if (strcmp(b->j3.spot, "XX") == 0 || strcmp(b->j3.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 4) {
      if (strcmp(b->j4.spot, "XX") == 0 || strcmp(b->j4.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 5) {
      if (strcmp(b->j5.spot, "XX") == 0 || strcmp(b->j5.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 6) {
      if (strcmp(b->j6.spot, "XX") == 0 || strcmp(b->j6.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 7) {
      if (strcmp(b->j7.spot, "XX") == 0 || strcmp(b->j7.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 8) {
      if (strcmp(b->j8.spot, "XX") == 0 || strcmp(b->j8.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 9) {
      if (strcmp(b->j9.spot, "XX") == 0 || strcmp(b->j9.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else if (num == 10) {
      if (strcmp(b->j10.spot, "XX") == 0 || strcmp(b->j10.spot, "MS") == 0) {

    check = 1;
      } else {

    check = 0;
      }
    } else {

      check = 2;
    }
  } else {

    check = 2;
  }

  return check;
}

/*Returns 1 if a hit, 0 if a miss*/
static int fire (Board * b1, Board * b2, char move, int num) {

  int hit;

  if (move == 'a' || move == 'A') {
    if (num == 1) {
      if (strcmp(b2->pa1.spot, "DS") == 0) {

    hit =1;
    b2->pa1.spot = "XX";
    b1->a1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa1.spot, "CR") == 0) {

    hit = 1;
    b2->pa1.spot = "XX";
    b1->a1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa1.spot, "SB") == 0) {

    hit = 1;
    b2->pa1.spot = "XX";
    b1->a1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa1.spot, "BS") == 0) {

    hit = 1;
    b2->pa1.spot = "XX";
    b1->a1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa1.spot, "AC") == 0) {

    hit = 1;
    b2->pa1.spot = "XX";
    b1->a1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pa1.spot = "MS";
    b1->a1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pa2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pa2.spot = "XX";
    b1->a2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pa2.spot = "XX";
    b1->a2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pa2.spot = "XX";
    b1->a2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pa2.spot = "XX";
    b1->a2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pa2.spot = "XX";
    b1->a2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pa2.spot = "MS";
    b1->a2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pa3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pa3.spot = "XX";
    b1->a3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pa3.spot = "XX";
    b1->a3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pa3.spot = "XX";
    b1->a3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pa3.spot = "XX";
    b1->a3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pa3.spot = "XX";
    b1->a3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pa3.spot = "MS";
    b1->a3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pa4.spot, "DS") == 0) {
    
    hit =1;
    b2->pa4.spot = "XX";
    b1->a4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pa4.spot = "XX";
    b1->a4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pa4.spot = "XX";
    b1->a4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pa4.spot = "XX";
    b1->a4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pa4.spot = "XX";
    b1->a4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pa4.spot = "MS";
    b1->a4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pa5.spot, "DS") == 0) {
    
    hit =1;
    b2->pa5.spot = "XX";
    b1->a5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pa5.spot = "XX";
    b1->a5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pa5.spot = "XX";
    b1->a5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa5.spot, "BS") == 0) {

    hit = 1;
    b2->pa5.spot = "XX";
    b1->a5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa5.spot, "AC") == 0) {

    hit = 1;
    b2->pa5.spot = "XX";
    b1->a5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pa5.spot = "MS";
    b1->a5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pa6.spot, "DS") == 0) {

    hit =1;
    b2->pa6.spot = "XX";
    b1->a6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa6.spot, "CR") == 0) {

    hit = 1;
    b2->pa6.spot = "XX";
    b1->a6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa6.spot, "SB") == 0) {

    hit = 1;
    b2->pa6.spot = "XX";
    b1->a6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa6.spot, "BS") == 0) {

    hit = 1;
    b2->pa6.spot = "XX";
    b1->a6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa6.spot, "AC") == 0) {

    hit = 1;
    b2->pa6.spot = "XX";
    b1->a6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pa6.spot = "MS";
    b1->a6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pa7.spot, "DS") == 0) {

    hit =1;
    b2->pa7.spot = "XX";
    b1->a7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa7.spot, "CR") == 0) {

    hit = 1;
    b2->pa7.spot = "XX";
    b1->a7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa7.spot, "SB") == 0) {

    hit = 1;
    b2->pa7.spot = "XX";
    b1->a7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa7.spot, "BS") == 0) {

    hit = 1;
    b2->pa7.spot = "XX";
    b1->a7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa7.spot, "AC") == 0) {

    hit = 1;
    b2->pa7.spot = "XX";
    b1->a7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pa7.spot = "MS";
    b1->a7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pa8.spot, "DS") == 0) {

    hit =1;
    b2->pa8.spot = "XX";
    b1->a8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa8.spot, "CR") == 0) {

    hit = 1;
    b2->pa8.spot = "XX";
    b1->a8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa8.spot, "SB") == 0) {

    hit = 1;
    b2->pa8.spot = "XX";
    b1->a8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa8.spot, "BS") == 0) {

    hit = 1;
    b2->pa8.spot = "XX";
    b1->a8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa8.spot, "AC") == 0) {

    hit = 1;
    b2->pa8.spot = "XX";
    b1->a8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pa8.spot = "MS";
    b1->a8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pa9.spot, "DS") == 0) {

    hit =1;
    b2->pa9.spot = "XX";
    b1->a9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa9.spot, "CR") == 0) {

    hit = 1;
    b2->pa9.spot = "XX";
    b1->a9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa9.spot, "SB") == 0) {

    hit = 1;
    b2->pa9.spot = "XX";
    b1->a9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa9.spot, "BS") == 0) {

    hit = 1;
    b2->pa9.spot = "XX";
    b1->a9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa9.spot, "AC") == 0) {

    hit = 1;
    b2->pa9.spot = "XX";
    b1->a9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pa9.spot = "MS";
    b1->a9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pa10.spot, "DS") == 0) {

    hit =1;
    b2->pa10.spot = "XX";
    b1->a10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pa10.spot, "CR") == 0) {

    hit = 1;
    b2->pa10.spot = "XX";
    b1->a10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pa10.spot, "SB") == 0) {

    hit = 1;
    b2->pa10.spot = "XX";
    b1->a10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pa10.spot, "BS") == 0) {

    hit = 1;
    b2->pa10.spot = "XX";
    b1->a10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pa10.spot, "AC") == 0) {

    hit = 1;
    b2->pa10.spot = "XX";
    b1->a10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pa10.spot = "MS";
    b1->a10.spot = "MS";
      }
    }
  } else if (move == 'b' || move == 'B') {
    if (num == 1) {
      if (strcmp(b2->pb1.spot, "DS") == 0) {

    hit =1;
    b2->pb1.spot = "XX";
    b1->b1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb1.spot, "CR") == 0) {

    hit = 1;
    b2->pb1.spot = "XX";
    b1->b1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb1.spot, "SB") == 0) {

    hit = 1;
    b2->pb1.spot = "XX";
    b1->b1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb1.spot, "BS") == 0) {

    hit = 1;
    b2->pb1.spot = "XX";
    b1->b1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb1.spot, "AC") == 0) {

    hit = 1;
    b2->pb1.spot = "XX";
    b1->b1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pb1.spot = "MS";
    b1->b1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pb2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pb2.spot = "XX";
    b1->b2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pb2.spot = "XX";
    b1->b2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pb2.spot = "XX";
    b1->b2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pb2.spot = "XX";
    b1->b2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pb2.spot = "XX";
    b1->b2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pb2.spot = "MS";
    b1->b2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pb3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pb3.spot = "XX";
    b1->b3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pb3.spot = "XX";
    b1->b3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pb3.spot = "XX";
    b1->b3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pb3.spot = "XX";
    b1->b3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pb3.spot = "XX";
    b1->b3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pb3.spot = "MS";
    b1->b3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pb4.spot, "DS") == 0) {
    
    hit =1;
    b2->pb4.spot = "XX";
    b1->b4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pb4.spot = "XX";
    b1->b4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pb4.spot = "XX";
    b1->b4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pb4.spot = "XX";
    b1->b4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pb4.spot = "XX";
    b1->b4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pb4.spot = "MS";
    b1->b4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pb5.spot, "DS") == 0) {
    
    hit =1;
    b2->pb5.spot = "XX";
    b1->b5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pb5.spot = "XX";
    b1->b5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pb5.spot = "XX";
    b1->b5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb5.spot, "BS") == 0) {

    hit = 1;
    b2->pb5.spot = "XX";
    b1->b5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb5.spot, "AC") == 0) {

    hit = 1;
    b2->pb5.spot = "XX";
    b1->b5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pb5.spot = "MS";
    b1->b5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pb6.spot, "DS") == 0) {

    hit =1;
    b2->pb6.spot = "XX";
    b1->b6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb6.spot, "CR") == 0) {

    hit = 1;
    b2->pb6.spot = "XX";
    b1->b6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb6.spot, "SB") == 0) {

    hit = 1;
    b2->pb6.spot = "XX";
    b1->b6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb6.spot, "BS") == 0) {

    hit = 1;
    b2->pb6.spot = "XX";
    b1->b6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb6.spot, "AC") == 0) {

    hit = 1;
    b2->pb6.spot = "XX";
    b1->b6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pb6.spot = "MS";
    b1->b6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pb7.spot, "DS") == 0) {

    hit =1;
    b2->pb7.spot = "XX";
    b1->b7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb7.spot, "CR") == 0) {

    hit = 1;
    b2->pb7.spot = "XX";
    b1->b7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb7.spot, "SB") == 0) {

    hit = 1;
    b2->pb7.spot = "XX";
    b1->b7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb7.spot, "BS") == 0) {

    hit = 1;
    b2->pb7.spot = "XX";
    b1->b7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb7.spot, "AC") == 0) {

    hit = 1;
    b2->pb7.spot = "XX";
    b1->b7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pb7.spot = "MS";
    b1->b7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pb8.spot, "DS") == 0) {

    hit =1;
    b2->pb8.spot = "XX";
    b1->b8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb8.spot, "CR") == 0) {

    hit = 1;
    b2->pb8.spot = "XX";
    b1->b8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb8.spot, "SB") == 0) {

    hit = 1;
    b2->pb8.spot = "XX";
    b1->b8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb8.spot, "BS") == 0) {

    hit = 1;
    b2->pb8.spot = "XX";
    b1->b8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb8.spot, "AC") == 0) {

    hit = 1;
    b2->pb8.spot = "XX";
    b1->b8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pb8.spot = "MS";
    b1->b8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pb9.spot, "DS") == 0) {

    hit =1;
    b2->pb9.spot = "XX";
    b1->b9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb9.spot, "CR") == 0) {

    hit = 1;
    b2->pb9.spot = "XX";
    b1->b9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb9.spot, "SB") == 0) {

    hit = 1;
    b2->pb9.spot = "XX";
    b1->b9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb9.spot, "BS") == 0) {

    hit = 1;
    b2->pb9.spot = "XX";
    b1->b9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb9.spot, "AC") == 0) {

    hit = 1;
    b2->pb9.spot = "XX";
    b1->b9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pb9.spot = "MS";
    b1->b9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pb10.spot, "DS") == 0) {

    hit =1;
    b2->pb10.spot = "XX";
    b1->b10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pb10.spot, "CR") == 0) {

    hit = 1;
    b2->pb10.spot = "XX";
    b1->b10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pb10.spot, "SB") == 0) {

    hit = 1;
    b2->pb10.spot = "XX";
    b1->b10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pb10.spot, "BS") == 0) {

    hit = 1;
    b2->pb10.spot = "XX";
    b1->b10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pb10.spot, "AC") == 0) {

    hit = 1;
    b2->pb10.spot = "XX";
    b1->b10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pb10.spot = "MS";
    b1->b10.spot = "MS";
      }
    }
  } else if (move == 'c' || move == 'C') {
    if (num == 1) {
      if (strcmp(b2->pc1.spot, "DS") == 0) {

    hit =1;
    b2->pc1.spot = "XX";
    b1->c1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc1.spot, "CR") == 0) {

    hit = 1;
    b2->pc1.spot = "XX";
    b1->c1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc1.spot, "SB") == 0) {

    hit = 1;
    b2->pc1.spot = "XX";
    b1->c1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc1.spot, "BS") == 0) {

    hit = 1;
    b2->pc1.spot = "XX";
    b1->c1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc1.spot, "AC") == 0) {

    hit = 1;
    b2->pc1.spot = "XX";
    b1->c1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pc1.spot = "MS";
    b1->c1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pc2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pc2.spot = "XX";
    b1->c2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pc2.spot = "XX";
    b1->c2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pc2.spot = "XX";
    b1->c2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pc2.spot = "XX";
    b1->c2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pc2.spot = "XX";
    b1->c2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pc2.spot = "MS";
    b1->c2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pc3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pc3.spot = "XX";
    b1->c3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pc3.spot = "XX";
    b1->c3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pc3.spot = "XX";
    b1->c3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pc3.spot = "XX";
    b1->c3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pc3.spot = "XX";
    b1->c3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pc3.spot = "MS";
    b1->c3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pc4.spot, "DS") == 0) {
    
    hit =1;
    b2->pc4.spot = "XX";
    b1->c4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pc4.spot = "XX";
    b1->c4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pc4.spot = "XX";
    b1->c4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pc4.spot = "XX";
    b1->c4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pc4.spot = "XX";
    b1->c4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pc4.spot = "MS";
    b1->c4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pc5.spot, "DS") == 0) {
    
    hit =1;
    b2->pc5.spot = "XX";
    b1->c5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pc5.spot = "XX";
    b1->c5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pc5.spot = "XX";
    b1->c5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc5.spot, "BS") == 0) {

    hit = 1;
    b2->pc5.spot = "XX";
    b1->c5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc5.spot, "AC") == 0) {

    hit = 1;
    b2->pc5.spot = "XX";
    b1->c5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pc5.spot = "MS";
    b1->c5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pc6.spot, "DS") == 0) {

    hit =1;
    b2->pc6.spot = "XX";
    b1->c6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc6.spot, "CR") == 0) {

    hit = 1;
    b2->pc6.spot = "XX";
    b1->c6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc6.spot, "SB") == 0) {

    hit = 1;
    b2->pc6.spot = "XX";
    b1->c6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc6.spot, "BS") == 0) {

    hit = 1;
    b2->pc6.spot = "XX";
    b1->c6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc6.spot, "AC") == 0) {

    hit = 1;
    b2->pc6.spot = "XX";
    b1->c6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pc6.spot = "MS";
    b1->c6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pc7.spot, "DS") == 0) {

    hit =1;
    b2->pc7.spot = "XX";
    b1->c7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc7.spot, "CR") == 0) {

    hit = 1;
    b2->pc7.spot = "XX";
    b1->c7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc7.spot, "SB") == 0) {

    hit = 1;
    b2->pc7.spot = "XX";
    b1->c7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc7.spot, "BS") == 0) {

    hit = 1;
    b2->pc7.spot = "XX";
    b1->c7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc7.spot, "AC") == 0) {

    hit = 1;
    b2->pc7.spot = "XX";
    b1->c7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pc7.spot = "MS";
    b1->c7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pc8.spot, "DS") == 0) {

    hit =1;
    b2->pc8.spot = "XX";
    b1->c8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc8.spot, "CR") == 0) {

    hit = 1;
    b2->pc8.spot = "XX";
    b1->c8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc8.spot, "SB") == 0) {

    hit = 1;
    b2->pc8.spot = "XX";
    b1->c8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc8.spot, "BS") == 0) {

    hit = 1;
    b2->pc8.spot = "XX";
    b1->c8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc8.spot, "AC") == 0) {

    hit = 1;
    b2->pc8.spot = "XX";
    b1->c8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pc8.spot = "MS";
    b1->c8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pc9.spot, "DS") == 0) {

    hit =1;
    b2->pc9.spot = "XX";
    b1->c9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc9.spot, "CR") == 0) {

    hit = 1;
    b2->pc9.spot = "XX";
    b1->c9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc9.spot, "SB") == 0) {

    hit = 1;
    b2->pc9.spot = "XX";
    b1->c9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc9.spot, "BS") == 0) {

    hit = 1;
    b2->pc9.spot = "XX";
    b1->c9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc9.spot, "AC") == 0) {

    hit = 1;
    b2->pc9.spot = "XX";
    b1->c9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pc9.spot = "MS";
    b1->c9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pc10.spot, "DS") == 0) {

    hit =1;
    b2->pc10.spot = "XX";
    b1->c10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pc10.spot, "CR") == 0) {

    hit = 1;
    b2->pc10.spot = "XX";
    b1->c10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pc10.spot, "SB") == 0) {

    hit = 1;
    b2->pc10.spot = "XX";
    b1->c10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pc10.spot, "BS") == 0) {

    hit = 1;
    b2->pc10.spot = "XX";
    b1->c10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pc10.spot, "AC") == 0) {

    hit = 1;
    b2->pc10.spot = "XX";
    b1->c10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pc10.spot = "MS";
    b1->c10.spot = "MS";
      }
    }
  } else if (move == 'd' || move == 'D') {
    if (num == 1) {
      if (strcmp(b2->pd1.spot, "DS") == 0) {

    hit =1;
    b2->pd1.spot = "XX";
    b1->d1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd1.spot, "CR") == 0) {

    hit = 1;
    b2->pd1.spot = "XX";
    b1->d1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd1.spot, "SB") == 0) {

    hit = 1;
    b2->pd1.spot = "XX";
    b1->d1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd1.spot, "BS") == 0) {

    hit = 1;
    b2->pd1.spot = "XX";
    b1->d1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd1.spot, "AC") == 0) {

    hit = 1;
    b2->pd1.spot = "XX";
    b1->d1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pd1.spot = "MS";
    b1->d1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pd2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pd2.spot = "XX";
    b1->d2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pd2.spot = "XX";
    b1->d2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pd2.spot = "XX";
    b1->d2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pd2.spot = "XX";
    b1->d2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pd2.spot = "XX";
    b1->d2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pd2.spot = "MS";
    b1->d2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pd3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pd3.spot = "XX";
    b1->d3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pd3.spot = "XX";
    b1->d3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pd3.spot = "XX";
    b1->d3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pd3.spot = "XX";
    b1->d3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pd3.spot = "XX";
    b1->d3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pd3.spot = "MS";
    b1->d3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pd4.spot, "DS") == 0) {
    
    hit = 1;
    b2->pd4.spot = "XX";
    b1->d4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pd4.spot = "XX";
    b1->d4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pd4.spot = "XX";
    b1->d4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pd4.spot = "XX";
    b1->d4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pd4.spot = "XX";
    b1->d4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pd4.spot = "MS";
    b1->d4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pd5.spot, "DS") == 0) {
    
    hit = 1;
    b2->pd5.spot = "XX";
    b1->d5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pd5.spot = "XX";
    b1->d5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pd5.spot = "XX";
    b1->d5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd5.spot, "BS") == 0) {

    hit = 1;
    b2->pd5.spot = "XX";
    b1->d5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd5.spot, "AC") == 0) {

    hit = 1;
    b2->pd5.spot = "XX";
    b1->d5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pd5.spot = "MS";
    b1->d5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pd6.spot, "DS") == 0) {

    hit = 1;
    b2->pd6.spot = "XX";
    b1->d6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd6.spot, "CR") == 0) {

    hit = 1;
    b2->pd6.spot = "XX";
    b1->d6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd6.spot, "SB") == 0) {

    hit = 1;
    b2->pd6.spot = "XX";
    b1->d6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd6.spot, "BS") == 0) {

    hit = 1;
    b2->pd6.spot = "XX";
    b1->d6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd6.spot, "AC") == 0) {

    hit = 1;
    b2->pd6.spot = "XX";
    b1->d6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pd6.spot = "MS";
    b1->d6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pd7.spot, "DS") == 0) {

    hit = 1;
    b2->pd7.spot = "XX";
    b1->d7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd7.spot, "CR") == 0) {

    hit = 1;
    b2->pd7.spot = "XX";
    b1->d7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd7.spot, "SB") == 0) {

    hit = 1;
    b2->pd7.spot = "XX";
    b1->d7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd7.spot, "BS") == 0) {

    hit = 1;
    b2->pd7.spot = "XX";
    b1->d7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd7.spot, "AC") == 0) {

    hit = 1;
    b2->pd7.spot = "XX";
    b1->d7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pd7.spot = "MS";
    b1->d7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pd8.spot, "DS") == 0) {

    hit = 1;
    b2->pd8.spot = "XX";
    b1->d8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd8.spot, "CR") == 0) {

    hit = 1;
    b2->pd8.spot = "XX";
    b1->d8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd8.spot, "SB") == 0) {

    hit = 1;
    b2->pd8.spot = "XX";
    b1->d8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd8.spot, "BS") == 0) {

    hit = 1;
    b2->pd8.spot = "XX";
    b1->d8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd8.spot, "AC") == 0) {

    hit = 1;
    b2->pd8.spot = "XX";
    b1->d8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pd8.spot = "MS";
    b1->d8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pd9.spot, "DS") == 0) {

    hit = 1;
    b2->pd9.spot = "XX";
    b1->d9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd9.spot, "CR") == 0) {

    hit = 1;
    b2->pd9.spot = "XX";
    b1->d9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd9.spot, "SB") == 0) {

    hit = 1;
    b2->pd9.spot = "XX";
    b1->d9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd9.spot, "BS") == 0) {

    hit = 1;
    b2->pd9.spot = "XX";
    b1->d9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd9.spot, "AC") == 0) {

    hit = 1;
    b2->pd9.spot = "XX";
    b1->d9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pd9.spot = "MS";
    b1->d9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pd10.spot, "DS") == 0) {

    hit =1;
    b2->pd10.spot = "XX";
    b1->d10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pd10.spot, "CR") == 0) {

    hit = 1;
    b2->pd10.spot = "XX";
    b1->d10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pd10.spot, "SB") == 0) {

    hit = 1;
    b2->pd10.spot = "XX";
    b1->d10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pd10.spot, "BS") == 0) {

    hit = 1;
    b2->pd10.spot = "XX";
    b1->d10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pd10.spot, "AC") == 0) {

    hit = 1;
    b2->pd10.spot = "XX";
    b1->d10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pd10.spot = "MS";
    b1->d10.spot = "MS";
      }
    }
  } else if (move == 'e' || move == 'E') {
    if (num == 1) {
      if (strcmp(b2->pe1.spot, "DS") == 0) {

    hit = 1;
    b2->pe1.spot = "XX";
    b1->e1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe1.spot, "CR") == 0) {

    hit = 1;
    b2->pe1.spot = "XX";
    b1->e1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe1.spot, "SB") == 0) {

    hit = 1;
    b2->pe1.spot = "XX";
    b1->e1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe1.spot, "BS") == 0) {

    hit = 1;
    b2->pe1.spot = "XX";
    b1->e1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe1.spot, "AC") == 0) {

    hit = 1;
    b2->pe1.spot = "XX";
    b1->e1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pe1.spot = "MS";
    b1->e1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pe2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pe2.spot = "XX";
    b1->e2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pe2.spot = "XX";
    b1->e2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pe2.spot = "XX";
    b1->e2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pe2.spot = "XX";
    b1->e2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pe2.spot = "XX";
    b1->e2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pe2.spot = "MS";
    b1->e2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pe3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pe3.spot = "XX";
    b1->e3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pe3.spot = "XX";
    b1->e3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pe3.spot = "XX";
    b1->e3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pe3.spot = "XX";
    b1->e3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pe3.spot = "XX";
    b1->e3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pe3.spot = "MS";
    b1->e3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pe4.spot, "DS") == 0) {
    
    hit = 1;
    b2->pe4.spot = "XX";
    b1->e4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pe4.spot = "XX";
    b1->e4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pe4.spot = "XX";
    b1->e4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pe4.spot = "XX";
    b1->e4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pe4.spot = "XX";
    b1->e4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pe4.spot = "MS";
    b1->e4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pe5.spot, "DS") == 0) {
    
    hit = 1;
    b2->pe5.spot = "XX";
    b1->e5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pe5.spot = "XX";
    b1->e5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pe5.spot = "XX";
    b1->e5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe5.spot, "BS") == 0) {

    hit = 1;
    b2->pe5.spot = "XX";
    b1->e5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe5.spot, "AC") == 0) {

    hit = 1;
    b2->pe5.spot = "XX";
    b1->e5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pe5.spot = "MS";
    b1->e5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pe6.spot, "DS") == 0) {

    hit = 1;
    b2->pe6.spot = "XX";
    b1->e6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe6.spot, "CR") == 0) {

    hit = 1;
    b2->pe6.spot = "XX";
    b1->e6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe6.spot, "SB") == 0) {

    hit = 1;
    b2->pe6.spot = "XX";
    b1->e6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe6.spot, "BS") == 0) {

    hit = 1;
    b2->pe6.spot = "XX";
    b1->e6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe6.spot, "AC") == 0) {

    hit = 1;
    b2->pe6.spot = "XX";
    b1->e6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pe6.spot = "MS";
    b1->e6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pe7.spot, "DS") == 0) {

    hit = 1;
    b2->pe7.spot = "XX";
    b1->e7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe7.spot, "CR") == 0) {

    hit = 1;
    b2->pe7.spot = "XX";
    b1->e7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe7.spot, "SB") == 0) {

    hit = 1;
    b2->pe7.spot = "XX";
    b1->e7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe7.spot, "BS") == 0) {

    hit = 1;
    b2->pe7.spot = "XX";
    b1->e7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe7.spot, "AC") == 0) {

    hit = 1;
    b2->pe7.spot = "XX";
    b1->e7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pe7.spot = "MS";
    b1->e7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pe8.spot, "DS") == 0) {

    hit = 1;
    b2->pe8.spot = "XX";
    b1->e8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe8.spot, "CR") == 0) {

    hit = 1;
    b2->pe8.spot = "XX";
    b1->e8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe8.spot, "SB") == 0) {

    hit = 1;
    b2->pe8.spot = "XX";
    b1->e8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe8.spot, "BS") == 0) {

    hit = 1;
    b2->pe8.spot = "XX";
    b1->e8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe8.spot, "AC") == 0) {

    hit = 1;
    b2->pe8.spot = "XX";
    b1->e8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pe8.spot = "MS";
    b1->e8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pe9.spot, "DS") == 0) {

    hit = 1;
    b2->pe9.spot = "XX";
    b1->e9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe9.spot, "CR") == 0) {

    hit = 1;
    b2->pe9.spot = "XX";
    b1->e9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe9.spot, "SB") == 0) {

    hit = 1;
    b2->pe9.spot = "XX";
    b1->e9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe9.spot, "BS") == 0) {

    hit = 1;
    b2->pe9.spot = "XX";
    b1->e9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe9.spot, "AC") == 0) {

    hit = 1;
    b2->pe9.spot = "XX";
    b1->e9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pe9.spot = "MS";
    b1->e9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pe10.spot, "DS") == 0) {

    hit = 1;
    b2->pe10.spot = "XX";
    b1->e10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pe10.spot, "CR") == 0) {

    hit = 1;
    b2->pe10.spot = "XX";
    b1->e10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pe10.spot, "SB") == 0) {

    hit = 1;
    b2->pe10.spot = "XX";
    b1->e10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pe10.spot, "BS") == 0) {

    hit = 1;
    b2->pe10.spot = "XX";
    b1->e10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pe10.spot, "AC") == 0) {

    hit = 1;
    b2->pe10.spot = "XX";
    b1->e10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pe10.spot = "MS";
    b1->e10.spot = "MS";
      }
    }
  } else if (move == 'f' || move == 'F') {
    if (num == 1) {
      if (strcmp(b2->pf1.spot, "DS") == 0) {

    hit = 1;
    b2->pf1.spot = "XX";
    b1->f1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf1.spot, "CR") == 0) {

    hit = 1;
    b2->pf1.spot = "XX";
    b1->f1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf1.spot, "SB") == 0) {

    hit = 1;
    b2->pf1.spot = "XX";
    b1->f1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf1.spot, "BS") == 0) {

    hit = 1;
    b2->pf1.spot = "XX";
    b1->f1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf1.spot, "AC") == 0) {

    hit = 1;
    b2->pf1.spot = "XX";
    b1->f1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pf1.spot = "MS";
    b1->f1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pf2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pf2.spot = "XX";
    b1->f2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pf2.spot = "XX";
    b1->f2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pf2.spot = "XX";
    b1->f2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pf2.spot = "XX";
    b1->f2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pf2.spot = "XX";
    b1->f2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pf2.spot = "MS";
    b1->f2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pf3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pf3.spot = "XX";
    b1->f3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pf3.spot = "XX";
    b1->f3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pf3.spot = "XX";
    b1->f3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pf3.spot = "XX";
    b1->f3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pf3.spot = "XX";
    b1->f3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pf3.spot = "MS";
    b1->f3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pf4.spot, "DS") == 0) {
    
    hit = 1;
    b2->pf4.spot = "XX";
    b1->f4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pf4.spot = "XX";
    b1->f4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pf4.spot = "XX";
    b1->f4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pf4.spot = "XX";
    b1->f4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pf4.spot = "XX";
    b1->f4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pf4.spot = "MS";
    b1->f4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pf5.spot, "DS") == 0) {
    
    hit = 1;
    b2->pf5.spot = "XX";
    b1->f5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pf5.spot = "XX";
    b1->f5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pf5.spot = "XX";
    b1->f5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf5.spot, "BS") == 0) {

    hit = 1;
    b2->pf5.spot = "XX";
    b1->f5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf5.spot, "AC") == 0) {

    hit = 1;
    b2->pf5.spot = "XX";
    b1->f5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pf5.spot = "MS";
    b1->f5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pf6.spot, "DS") == 0) {

    hit = 1;
    b2->pf6.spot = "XX";
    b1->f6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf6.spot, "CR") == 0) {

    hit = 1;
    b2->pf6.spot = "XX";
    b1->f6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf6.spot, "SB") == 0) {

    hit = 1;
    b2->pf6.spot = "XX";
    b1->f6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf6.spot, "BS") == 0) {

    hit = 1;
    b2->pf6.spot = "XX";
    b1->f6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf6.spot, "AC") == 0) {

    hit = 1;
    b2->pf6.spot = "XX";
    b1->f6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pf6.spot = "MS";
    b1->f6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pf7.spot, "DS") == 0) {

    hit = 1;
    b2->pf7.spot = "XX";
    b1->f7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf7.spot, "CR") == 0) {

    hit = 1;
    b2->pf7.spot = "XX";
    b1->f7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf7.spot, "SB") == 0) {

    hit = 1;
    b2->pf7.spot = "XX";
    b1->f7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf7.spot, "BS") == 0) {

    hit = 1;
    b2->pf7.spot = "XX";
    b1->f7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf7.spot, "AC") == 0) {

    hit = 1;
    b2->pf7.spot = "XX";
    b1->f7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pf7.spot = "MS";
    b1->f7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pf8.spot, "DS") == 0) {

    hit = 1;
    b2->pf8.spot = "XX";
    b1->f8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf8.spot, "CR") == 0) {

    hit = 1;
    b2->pf8.spot = "XX";
    b1->f8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf8.spot, "SB") == 0) {

    hit = 1;
    b2->pf8.spot = "XX";
    b1->f8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf8.spot, "BS") == 0) {

    hit = 1;
    b2->pf8.spot = "XX";
    b1->f8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf8.spot, "AC") == 0) {

    hit = 1;
    b2->pf8.spot = "XX";
    b1->f8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pf8.spot = "MS";
    b1->f8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pf9.spot, "DS") == 0) {

    hit = 1;
    b2->pf9.spot = "XX";
    b1->f9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf9.spot, "CR") == 0) {

    hit = 1;
    b2->pf9.spot = "XX";
    b1->f9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf9.spot, "SB") == 0) {

    hit = 1;
    b2->pf9.spot = "XX";
    b1->f9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf9.spot, "BS") == 0) {

    hit = 1;
    b2->pf9.spot = "XX";
    b1->f9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf9.spot, "AC") == 0) {

    hit = 1;
    b2->pf9.spot = "XX";
    b1->f9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pf9.spot = "MS";
    b1->f9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pf10.spot, "DS") == 0) {

    hit = 1;
    b2->pf10.spot = "XX";
    b1->f10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pf10.spot, "CR") == 0) {

    hit = 1;
    b2->pf10.spot = "XX";
    b1->f10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pf10.spot, "SB") == 0) {

    hit = 1;
    b2->pf10.spot = "XX";
    b1->f10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pf10.spot, "BS") == 0) {

    hit = 1;
    b2->pf10.spot = "XX";
    b1->f10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pf10.spot, "AC") == 0) {

    hit = 1;
    b2->pf10.spot = "XX";
    b1->f10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pf10.spot = "MS";
    b1->f10.spot = "MS";
      }
    }
  } else if (move == 'g' || move == 'G') {
    if (num == 1) {
      if (strcmp(b2->pg1.spot, "DS") == 0) {

    hit = 1;
    b2->pg1.spot = "XX";
    b1->g1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg1.spot, "CR") == 0) {

    hit = 1;
    b2->pg1.spot = "XX";
    b1->g1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg1.spot, "SB") == 0) {

    hit = 1;
    b2->pg1.spot = "XX";
    b1->g1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg1.spot, "BS") == 0) {

    hit = 1;
    b2->pg1.spot = "XX";
    b1->g1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg1.spot, "AC") == 0) {

    hit = 1;
    b2->pg1.spot = "XX";
    b1->g1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pg1.spot = "MS";
    b1->g1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pg2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pg2.spot = "XX";
    b1->g2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pg2.spot = "XX";
    b1->g2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pg2.spot = "XX";
    b1->g2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pg2.spot = "XX";
    b1->g2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pg2.spot = "XX";
    b1->g2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pg2.spot = "MS";
    b1->g2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pg3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pg3.spot = "XX";
    b1->g3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pg3.spot = "XX";
    b1->g3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pg3.spot = "XX";
    b1->g3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pg3.spot = "XX";
    b1->g3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pg3.spot = "XX";
    b1->g3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pg3.spot = "MS";
    b1->g3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pg4.spot, "DS") == 0) {
    
    hit = 1;
    b2->pg4.spot = "XX";
    b1->g4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pg4.spot = "XX";
    b1->g4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pg4.spot = "XX";
    b1->g4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pg4.spot = "XX";
    b1->g4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pg4.spot = "XX";
    b1->g4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pg4.spot = "MS";
    b1->g4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pg5.spot, "DS") == 0) {
    
    hit = 1;
    b2->pg5.spot = "XX";
    b1->g5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pg5.spot = "XX";
    b1->g5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pg5.spot = "XX";
    b1->g5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg5.spot, "BS") == 0) {

    hit = 1;
    b2->pg5.spot = "XX";
    b1->g5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg5.spot, "AC") == 0) {

    hit = 1;
    b2->pg5.spot = "XX";
    b1->g5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pg5.spot = "MS";
    b1->g5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pg6.spot, "DS") == 0) {

    hit = 1;
    b2->pg6.spot = "XX";
    b1->g6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg6.spot, "CR") == 0) {

    hit = 1;
    b2->pg6.spot = "XX";
    b1->g6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg6.spot, "SB") == 0) {

    hit = 1;
    b2->pg6.spot = "XX";
    b1->g6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg6.spot, "BS") == 0) {

    hit = 1;
    b2->pg6.spot = "XX";
    b1->g6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg6.spot, "AC") == 0) {

    hit = 1;
    b2->pg6.spot = "XX";
    b1->g6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pg6.spot = "MS";
    b1->g6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pg7.spot, "DS") == 0) {

    hit = 1;
    b2->pg7.spot = "XX";
    b1->g7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg7.spot, "CR") == 0) {

    hit = 1;
    b2->pg7.spot = "XX";
    b1->g7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg7.spot, "SB") == 0) {

    hit = 1;
    b2->pg7.spot = "XX";
    b1->g7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg7.spot, "BS") == 0) {

    hit = 1;
    b2->pg7.spot = "XX";
    b1->g7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg7.spot, "AC") == 0) {

    hit = 1;
    b2->pg7.spot = "XX";
    b1->g7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pg7.spot = "MS";
    b1->g7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pg8.spot, "DS") == 0) {

    hit = 1;
    b2->pg8.spot = "XX";
    b1->g8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg8.spot, "CR") == 0) {

    hit = 1;
    b2->pg8.spot = "XX";
    b1->g8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg8.spot, "SB") == 0) {

    hit = 1;
    b2->pg8.spot = "XX";
    b1->g8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg8.spot, "BS") == 0) {

    hit = 1;
    b2->pg8.spot = "XX";
    b1->g8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg8.spot, "AC") == 0) {

    hit = 1;
    b2->pg8.spot = "XX";
    b1->g8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pg8.spot = "MS";
    b1->g8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pg9.spot, "DS") == 0) {

    hit = 1;
    b2->pg9.spot = "XX";
    b1->g9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg9.spot, "CR") == 0) {

    hit = 1;
    b2->pg9.spot = "XX";
    b1->g9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg9.spot, "SB") == 0) {

    hit = 1;
    b2->pg9.spot = "XX";
    b1->g9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg9.spot, "BS") == 0) {

    hit = 1;
    b2->pg9.spot = "XX";
    b1->g9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg9.spot, "AC") == 0) {

    hit = 1;
    b2->pg9.spot = "XX";
    b1->g9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pg9.spot = "MS";
    b1->g9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pg10.spot, "DS") == 0) {

    hit = 1;
    b2->pg10.spot = "XX";
    b1->g10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pg10.spot, "CR") == 0) {

    hit = 1;
    b2->pg10.spot = "XX";
    b1->g10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pg10.spot, "SB") == 0) {

    hit = 1;
    b2->pg10.spot = "XX";
    b1->g10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pg10.spot, "BS") == 0) {

    hit = 1;
    b2->pg10.spot = "XX";
    b1->g10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pg10.spot, "AC") == 0) {

    hit = 1;
    b2->pg10.spot = "XX";
    b1->g10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pg10.spot = "MS";
    b1->g10.spot = "MS";
      }
    }
  } else if (move == 'h' || move == 'H') {
    if (num == 1) {
      if (strcmp(b2->ph1.spot, "DS") == 0) {

    hit = 1;
    b2->ph1.spot = "XX";
    b1->h1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph1.spot, "CR") == 0) {

    hit = 1;
    b2->ph1.spot = "XX";
    b1->h1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph1.spot, "SB") == 0) {

    hit = 1;
    b2->ph1.spot = "XX";
    b1->h1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph1.spot, "BS") == 0) {

    hit = 1;
    b2->ph1.spot = "XX";
    b1->h1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph1.spot, "AC") == 0) {

    hit = 1;
    b2->ph1.spot = "XX";
    b1->h1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->ph1.spot = "MS";
    b1->h1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->ph2.spot, "DS") == 0) {
    
    hit = 1;
    b2->ph2.spot = "XX";
    b1->h2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph2.spot, "CR") == 0) {
    
    hit = 1;
    b2->ph2.spot = "XX";
    b1->h2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph2.spot, "SB") == 0) {
    
    hit = 1;
    b2->ph2.spot = "XX";
    b1->h2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph2.spot, "BS") == 0) {
    
    hit = 1;
    b2->ph2.spot = "XX";
    b1->h2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph2.spot, "AC") == 0) {
    
    hit = 1;
    b2->ph2.spot = "XX";
    b1->h2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->ph2.spot = "MS";
    b1->h2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->ph3.spot, "DS") == 0) {
    
    hit = 1;
    b2->ph3.spot = "XX";
    b1->h3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph3.spot, "CR") == 0) {
    
    hit = 1;
    b2->ph3.spot = "XX";
    b1->h3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph3.spot, "SB") == 0) {
    
    hit = 1;
    b2->ph3.spot = "XX";
    b1->h3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph3.spot, "BS") == 0) {
    
    hit = 1;
    b2->ph3.spot = "XX";
    b1->h3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph3.spot, "AC") == 0) {
    
    hit = 1;
    b2->ph3.spot = "XX";
    b1->h3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->ph3.spot = "MS";
    b1->h3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->ph4.spot, "DS") == 0) {
    
    hit = 1;
    b2->ph4.spot = "XX";
    b1->h4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph4.spot, "CR") == 0) {
    
    hit = 1;
    b2->ph4.spot = "XX";
    b1->h4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph4.spot, "SB") == 0) {
    
    hit = 1;
    b2->ph4.spot = "XX";
    b1->h4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph4.spot, "BS") == 0) {
    
    hit = 1;
    b2->ph4.spot = "XX";
    b1->h4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph4.spot, "AC") == 0) {
    
    hit = 1;
    b2->ph4.spot = "XX";
    b1->h4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->ph4.spot = "MS";
    b1->h4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->ph5.spot, "DS") == 0) {
    
    hit = 1;
    b2->ph5.spot = "XX";
    b1->h5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph5.spot, "CR") == 0) {
    
    hit = 1;
    b2->ph5.spot = "XX";
    b1->h5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph5.spot, "SB") == 0) {
    
    hit = 1;
    b2->ph5.spot = "XX";
    b1->h5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph5.spot, "BS") == 0) {

    hit = 1;
    b2->ph5.spot = "XX";
    b1->h5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph5.spot, "AC") == 0) {

    hit = 1;
    b2->ph5.spot = "XX";
    b1->h5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->ph5.spot = "MS";
    b1->h5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->ph6.spot, "DS") == 0) {

    hit =1;
    b2->ph6.spot = "XX";
    b1->h6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph6.spot, "CR") == 0) {

    hit = 1;
    b2->ph6.spot = "XX";
    b1->h6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph6.spot, "SB") == 0) {

    hit = 1;
    b2->ph6.spot = "XX";
    b1->h6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph6.spot, "BS") == 0) {

    hit = 1;
    b2->ph6.spot = "XX";
    b1->h6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph6.spot, "AC") == 0) {

    hit = 1;
    b2->ph6.spot = "XX";
    b1->h6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->ph6.spot = "MS";
    b1->h6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->ph7.spot, "DS") == 0) {

    hit = 1;
    b2->ph7.spot = "XX";
    b1->h7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph7.spot, "CR") == 0) {

    hit = 1;
    b2->ph7.spot = "XX";
    b1->h7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph7.spot, "SB") == 0) {

    hit = 1;
    b2->ph7.spot = "XX";
    b1->h7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph7.spot, "BS") == 0) {

    hit = 1;
    b2->ph7.spot = "XX";
    b1->h7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph7.spot, "AC") == 0) {

    hit = 1;
    b2->ph7.spot = "XX";
    b1->h7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->ph7.spot = "MS";
    b1->h7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->ph8.spot, "DS") == 0) {

    hit = 1;
    b2->ph8.spot = "XX";
    b1->h8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph8.spot, "CR") == 0) {

    hit = 1;
    b2->ph8.spot = "XX";
    b1->h8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph8.spot, "SB") == 0) {

    hit = 1;
    b2->ph8.spot = "XX";
    b1->h8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph8.spot, "BS") == 0) {

    hit = 1;
    b2->ph8.spot = "XX";
    b1->h8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph8.spot, "AC") == 0) {

    hit = 1;
    b2->ph8.spot = "XX";
    b1->h8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->ph8.spot = "MS";
    b1->h8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->ph9.spot, "DS") == 0) {

    hit = 1;
    b2->ph9.spot = "XX";
    b1->h9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph9.spot, "CR") == 0) {

    hit = 1;
    b2->ph9.spot = "XX";
    b1->h9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph9.spot, "SB") == 0) {

    hit = 1;
    b2->ph9.spot = "XX";
    b1->h9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph9.spot, "BS") == 0) {

    hit = 1;
    b2->ph9.spot = "XX";
    b1->h9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph9.spot, "AC") == 0) {

    hit = 1;
    b2->ph9.spot = "XX";
    b1->h9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->ph9.spot = "MS";
    b1->h9.spot = "MS";
      }
    } else {
      if (strcmp(b2->ph10.spot, "DS") == 0) {

    hit = 1;
    b2->ph10.spot = "XX";
    b1->h10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->ph10.spot, "CR") == 0) {

    hit = 1;
    b2->ph10.spot = "XX";
    b1->h10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->ph10.spot, "SB") == 0) {

    hit = 1;
    b2->ph10.spot = "XX";
    b1->h10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->ph10.spot, "BS") == 0) {

    hit = 1;
    b2->ph10.spot = "XX";
    b1->h10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->ph10.spot, "AC") == 0) {

    hit = 1;
    b2->ph10.spot = "XX";
    b1->h10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->ph10.spot = "MS";
    b1->h10.spot = "MS";
      }
    }
  } else if (move == 'i' || move == 'I') {
    if (num == 1) {
      if (strcmp(b2->pi1.spot, "DS") == 0) {

    hit = 1;
    b2->pi1.spot = "XX";
    b1->i1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi1.spot, "CR") == 0) {

    hit = 1;
    b2->pi1.spot = "XX";
    b1->i1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi1.spot, "SB") == 0) {

    hit = 1;
    b2->pi1.spot = "XX";
    b1->i1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi1.spot, "BS") == 0) {

    hit = 1;
    b2->pi1.spot = "XX";
    b1->i1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi1.spot, "AC") == 0) {

    hit = 1;
    b2->pi1.spot = "XX";
    b1->i1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pi1.spot = "MS";
    b1->i1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pi2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pi2.spot = "XX";
    b1->i2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pi2.spot = "XX";
    b1->i2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pi2.spot = "XX";
    b1->i2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pi2.spot = "XX";
    b1->i2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pi2.spot = "XX";
    b1->i2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pi2.spot = "MS";
    b1->i2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pi3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pi3.spot = "XX";
    b1->i3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pi3.spot = "XX";
    b1->i3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pi3.spot = "XX";
    b1->i3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pi3.spot = "XX";
    b1->i3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pi3.spot = "XX";
    b1->i3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pi3.spot = "MS";
    b1->i3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pi4.spot, "DS") == 0) {
    
    hit = 1;
    b2->pi4.spot = "XX";
    b1->i4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pi4.spot = "XX";
    b1->i4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pi4.spot = "XX";
    b1->i4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pi4.spot = "XX";
    b1->i4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pi4.spot = "XX";
    b1->i4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pi4.spot = "MS";
    b1->i4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pi5.spot, "DS") == 0) {
    
    hit = 1;
    b2->pi5.spot = "XX";
    b1->i5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pi5.spot = "XX";
    b1->i5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pi5.spot = "XX";
    b1->i5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi5.spot, "BS") == 0) {

    hit = 1;
    b2->pi5.spot = "XX";
    b1->i5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi5.spot, "AC") == 0) {

    hit = 1;
    b2->pi5.spot = "XX";
    b1->i5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pi5.spot = "MS";
    b1->i5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pi6.spot, "DS") == 0) {

    hit = 1;
    b2->pi6.spot = "XX";
    b1->i6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi6.spot, "CR") == 0) {

    hit = 1;
    b2->pi6.spot = "XX";
    b1->i6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi6.spot, "SB") == 0) {

    hit = 1;
    b2->pi6.spot = "XX";
    b1->i6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi6.spot, "BS") == 0) {

    hit = 1;
    b2->pi6.spot = "XX";
    b1->i6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi6.spot, "AC") == 0) {

    hit = 1;
    b2->pi6.spot = "XX";
    b1->i6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pi6.spot = "MS";
    b1->i6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pi7.spot, "DS") == 0) {

    hit = 1;
    b2->pi7.spot = "XX";
    b1->i7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi7.spot, "CR") == 0) {

    hit = 1;
    b2->pi7.spot = "XX";
    b1->i7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi7.spot, "SB") == 0) {

    hit = 1;
    b2->pi7.spot = "XX";
    b1->i7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi7.spot, "BS") == 0) {

    hit = 1;
    b2->pi7.spot = "XX";
    b1->i7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi7.spot, "AC") == 0) {

    hit = 1;
    b2->pi7.spot = "XX";
    b1->i7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pi7.spot = "MS";
    b1->i7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pi8.spot, "DS") == 0) {

    hit = 1;
    b2->pi8.spot = "XX";
    b1->i8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi8.spot, "CR") == 0) {

    hit = 1;
    b2->pi8.spot = "XX";
    b1->i8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi8.spot, "SB") == 0) {

    hit = 1;
    b2->pi8.spot = "XX";
    b1->i8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi8.spot, "BS") == 0) {

    hit = 1;
    b2->pi8.spot = "XX";
    b1->i8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi8.spot, "AC") == 0) {

    hit = 1;
    b2->pi8.spot = "XX";
    b1->i8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pi8.spot = "MS";
    b1->i8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pi9.spot, "DS") == 0) {

    hit = 1;
    b2->pi9.spot = "XX";
    b1->i9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi9.spot, "CR") == 0) {

    hit = 1;
    b2->pi9.spot = "XX";
    b1->i9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi9.spot, "SB") == 0) {

    hit = 1;
    b2->pi9.spot = "XX";
    b1->i9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi9.spot, "BS") == 0) {

    hit = 1;
    b2->pi9.spot = "XX";
    b1->i9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi9.spot, "AC") == 0) {

    hit = 1;
    b2->pi9.spot = "XX";
    b1->i9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pi9.spot = "MS";
    b1->i9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pi10.spot, "DS") == 0) {

    hit = 1;
    b2->pi10.spot = "XX";
    b1->i10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pi10.spot, "CR") == 0) {

    hit = 1;
    b2->pi10.spot = "XX";
    b1->i10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pi10.spot, "SB") == 0) {

    hit = 1;
    b2->pi10.spot = "XX";
    b1->i10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pi10.spot, "BS") == 0) {

    hit = 1;
    b2->pi10.spot = "XX";
    b1->i10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pi10.spot, "AC") == 0) {

    hit = 1;
    b2->pi10.spot = "XX";
    b1->i10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pi10.spot = "MS";
    b1->i10.spot = "MS";
      }
    }
  } else {
    if (num == 1) {
      if (strcmp(b2->pj1.spot, "DS") == 0) {

    hit = 1;
    b2->pj1.spot = "XX";
    b1->j1.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj1.spot, "CR") == 0) {

    hit = 1;
    b2->pj1.spot = "XX";
    b1->j1.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj1.spot, "SB") == 0) {

    hit = 1;
    b2->pj1.spot = "XX";
    b1->j1.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj1.spot, "BS") == 0) {

    hit = 1;
    b2->pj1.spot = "XX";
    b1->j1.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj1.spot, "AC") == 0) {

    hit = 1;
    b2->pj1.spot = "XX";
    b1->j1.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pj1.spot = "MS";
    b1->j1.spot = "MS";
      }
    } else if (num == 2) {
      if (strcmp(b2->pj2.spot, "DS") == 0) {
    
    hit = 1;
    b2->pj2.spot = "XX";
    b1->j2.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj2.spot, "CR") == 0) {
    
    hit = 1;
    b2->pj2.spot = "XX";
    b1->j2.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj2.spot, "SB") == 0) {
    
    hit = 1;
    b2->pj2.spot = "XX";
    b1->j2.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj2.spot, "BS") == 0) {
    
    hit = 1;
    b2->pj2.spot = "XX";
    b1->j2.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj2.spot, "AC") == 0) {
    
    hit = 1;
    b2->pj2.spot = "XX";
    b1->j2.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pj2.spot = "MS";
    b1->j2.spot = "MS";
      }
    } else if (num == 3) {
      if (strcmp(b2->pj3.spot, "DS") == 0) {
    
    hit = 1;
    b2->pj3.spot = "XX";
    b1->j3.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj3.spot, "CR") == 0) {
    
    hit = 1;
    b2->pj3.spot = "XX";
    b1->j3.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj3.spot, "SB") == 0) {
    
    hit = 1;
    b2->pj3.spot = "XX";
    b1->j3.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj3.spot, "BS") == 0) {
    
    hit = 1;
    b2->pj3.spot = "XX";
    b1->j3.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj3.spot, "AC") == 0) {
    
    hit = 1;
    b2->pj3.spot = "XX";
    b1->j3.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pj3.spot = "MS";
    b1->j3.spot = "MS";
      }
    } else if (num == 4) {
      if (strcmp(b2->pj4.spot, "DS") == 0) {
    
    hit = 1;
    b2->pj4.spot = "XX";
    b1->j4.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj4.spot, "CR") == 0) {
    
    hit = 1;
    b2->pj4.spot = "XX";
    b1->j4.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj4.spot, "SB") == 0) {
    
    hit = 1;
    b2->pj4.spot = "XX";
    b1->j4.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj4.spot, "BS") == 0) {
    
    hit = 1;
    b2->pj4.spot = "XX";
    b1->j4.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj4.spot, "AC") == 0) {
    
    hit = 1;
    b2->pj4.spot = "XX";
    b1->j4.spot = "XX";
    b2->ac--;
      } else {
    
    hit = 0;
    b2->pj4.spot = "MS";
    b1->j4.spot = "MS";
      }
    } else if (num == 5) {
      if (strcmp(b2->pj5.spot, "DS") == 0) {
    
    hit = 1;
    b2->pj5.spot = "XX";
    b1->j5.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj5.spot, "CR") == 0) {
    
    hit = 1;
    b2->pj5.spot = "XX";
    b1->j5.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj5.spot, "SB") == 0) {
    
    hit = 1;
    b2->pj5.spot = "XX";
    b1->j5.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj5.spot, "BS") == 0) {

    hit = 1;
    b2->pj5.spot = "XX";
    b1->j5.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj5.spot, "AC") == 0) {

    hit = 1;
    b2->pj5.spot = "XX";
    b1->j5.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pj5.spot = "MS";
    b1->j5.spot = "MS";
      }
    } else if (num == 6) {
      if (strcmp(b2->pj6.spot, "DS") == 0) {

    hit = 1;
    b2->pj6.spot = "XX";
    b1->j6.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj6.spot, "CR") == 0) {

    hit = 1;
    b2->pj6.spot = "XX";
    b1->j6.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj6.spot, "SB") == 0) {

    hit = 1;
    b2->pj6.spot = "XX";
    b1->j6.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj6.spot, "BS") == 0) {

    hit = 1;
    b2->pj6.spot = "XX";
    b1->j6.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj6.spot, "AC") == 0) {

    hit = 1;
    b2->pj6.spot = "XX";
    b1->j6.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pj6.spot = "MS";
    b1->j6.spot = "MS";
      }
    } else if (num == 7) {
      if (strcmp(b2->pj7.spot, "DS") == 0) {

    hit = 1;
    b2->pj7.spot = "XX";
    b1->j7.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj7.spot, "CR") == 0) {

    hit = 1;
    b2->pj7.spot = "XX";
    b1->j7.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj7.spot, "SB") == 0) {

    hit = 1;
    b2->pj7.spot = "XX";
    b1->j7.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj7.spot, "BS") == 0) {

    hit = 1;
    b2->pj7.spot = "XX";
    b1->j7.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj7.spot, "AC") == 0) {

    hit = 1;
    b2->pj7.spot = "XX";
    b1->j7.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pj7.spot = "MS";
    b1->j7.spot = "MS";
      }
    } else if (num == 8) {
      if (strcmp(b2->pj8.spot, "DS") == 0) {

    hit = 1;
    b2->pj8.spot = "XX";
    b1->j8.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj8.spot, "CR") == 0) {

    hit = 1;
    b2->pj8.spot = "XX";
    b1->j8.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj8.spot, "SB") == 0) {

    hit = 1;
    b2->pj8.spot = "XX";
    b1->j8.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj8.spot, "BS") == 0) {

    hit = 1;
    b2->pj8.spot = "XX";
    b1->j8.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj8.spot, "AC") == 0) {

    hit = 1;
    b2->pj8.spot = "XX";
    b1->j8.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pj8.spot = "MS";
    b1->j8.spot = "MS";
      }
    } else if (num == 9) {
      if (strcmp(b2->pj9.spot, "DS") == 0) {

    hit = 1;
    b2->pj9.spot = "XX";
    b1->j9.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj9.spot, "CR") == 0) {

    hit = 1;
    b2->pj9.spot = "XX";
    b1->j9.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj9.spot, "SB") == 0) {

    hit = 1;
    b2->pj9.spot = "XX";
    b1->j9.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj9.spot, "BS") == 0) {

    hit = 1;
    b2->pj9.spot = "XX";
    b1->j9.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj9.spot, "AC") == 0) {

    hit = 1;
    b2->pj9.spot = "XX";
    b1->j9.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pj9.spot = "MS";
    b1->j9.spot = "MS";
      }
    } else {
      if (strcmp(b2->pj10.spot, "DS") == 0) {

    hit = 1;
    b2->pj10.spot = "XX";
    b1->j10.spot = "XX";
    b2->ds--;
      } else if (strcmp(b2->pj10.spot, "CR") == 0) {

    hit = 1;
    b2->pj10.spot = "XX";
    b1->j10.spot = "XX";
    b2->cr--;
      } else if (strcmp(b2->pj10.spot, "SB") == 0) {

    hit = 1;
    b2->pj10.spot = "XX";
    b1->j10.spot = "XX";
    b2->sb--;
      } else if (strcmp(b2->pj10.spot, "BS") == 0) {

    hit = 1;
    b2->pj10.spot = "XX";
    b1->j10.spot = "XX";
    b2->bs--;
      } else if (strcmp(b2->pj10.spot, "AC") == 0) {

    hit = 1;
    b2->pj10.spot = "XX";
    b1->j10.spot = "XX";
    b2->ac--;
      } else {

    hit = 0;
    b2->pj10.spot = "MS";
    b1->j10.spot = "MS";
      }
    }
  }

  return hit;
}

/*Returns 2 if a ship was destroyed, 1 if game over, 0 if not*/
static int check_victory_comp (Board * b) {

  int victory = 0;

  if (b->ds == 0) {

    victory = 2;
    b->ds = -1;
  }
  if (b->cr == 0) {

    victory = 2;
    b->cr = -1;
  }
  if (b->sb == 0) {

    victory = 2;
    b->sb = -1;
  }
  if (b->bs == 0) {

    victory = 2;
    b->bs = -1;
  }
  if (b->ac == 0) {

    victory = 2;
    b->ac = -1;
  }

  if (b->ac == -1 && b->bs == -1 && b->sb == -1 && b->cr == -1 && b->ds == -1) {

    victory = 1;
  } else if (victory != 2) {

    victory = 0;
  }

  return victory;
}

/*Returns 0 if game over*/
static int check_victory (Board * b) {

  int victory = 0;

  if (b->ds == 0) {

    printf("Their Destroyer is Down.\n");
    b->ds = -1;
  }
  if (b->cr == 0) {

    printf("Their Cruiser is Down.\n");
    b->cr = -1;
  }
  if (b->sb == 0) {

    printf("Their Submarine is Down.\n");
    b->sb = -1;
  }
  if (b->bs == 0) {

    printf("Their BattleShip is Down.\n");
    b->bs = -1;
  }
  if (b->ac == 0) {

    printf("Their Aircraft Carrier is Down.\n");
    b->ac = -1;
  }

  if (b->ds < 0) {

    victory += 0;
  } else {

    victory += b->ds;
  }

  if (b->cr < 0) {

    victory += 0;
  } else {

    victory += b->cr;
  }

  if (b->sb < 0) {

    victory += 0;
  } else {

    victory += b->sb;
  }

  if (b->bs < 0) {

    victory += 0;
  } else {

    victory += b->bs;
  }

  if (b->ac < 0) {

    victory += 0;
  } else {

    victory += b->ac;
  }
  return !victory;
}

static int check_victory_t (Board * b) {

  return !(b->sb);
}

static int random(int mod) {

  int r;

  srand(time(NULL));
  r = (rand() % mod) + 1;

  return r;
}

static void print_board (Board * b) {

  printf("\n%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->a1.spot, b->a2.spot, b->a3.spot, b->a4.spot, b->a5.spot, b->a6.spot, b->a7.spot, b->a8.spot, b->a9.spot, b->a10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->b1.spot, b->b2.spot, b->b3.spot, b->b4.spot, b->b5.spot, b->b6.spot, b->b7.spot, b->b8.spot, b->b9.spot, b->b10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->c1.spot, b->c2.spot, b->c3.spot, b->c4.spot, b->c5.spot, b->c6.spot, b->c7.spot, b->c8.spot, b->c9.spot, b->c10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->d1.spot, b->d2.spot, b->d3.spot, b->d4.spot, b->d5.spot, b->d6.spot, b->d7.spot, b->d8.spot, b->d9.spot, b->d10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->e1.spot, b->e2.spot, b->e3.spot, b->e4.spot, b->e5.spot, b->e6.spot, b->e7.spot, b->e8.spot, b->e9.spot, b->e10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->f1.spot, b->f2.spot, b->f3.spot, b->f4.spot, b->f5.spot, b->f6.spot, b->f7.spot, b->f8.spot, b->f9.spot, b->f10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->g1.spot, b->g2.spot, b->g3.spot, b->g4.spot, b->g5.spot, b->g6.spot, b->g7.spot, b->g8.spot, b->g9.spot, b->g10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->h1.spot, b->h2.spot, b->h3.spot, b->h4.spot, b->h5.spot, b->h6.spot, b->h7.spot, b->h8.spot, b->h9.spot, b->h10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->i1.spot, b->i2.spot, b->i3.spot, b->i4.spot, b->i5.spot, b->i6.spot, b->i7.spot, b->i8.spot, b->i9.spot, b->i10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->j1.spot, b->j2.spot, b->j3.spot, b->j4.spot, b->j5.spot, b->j6.spot, b->j7.spot, b->j8.spot, b->j9.spot, b->j10.spot);


  printf("\n\n");


  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pa1.spot, b->pa2.spot, b->pa3.spot, b->pa4.spot, b->pa5.spot, b->pa6.spot, b->pa7.spot, b->pa8.spot, b->pa9.spot, b->pa10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pb1.spot, b->pb2.spot, b->pb3.spot, b->pb4.spot, b->pb5.spot, b->pb6.spot, b->pb7.spot, b->pb8.spot, b->pb9.spot, b->pb10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pc1.spot, b->pc2.spot, b->pc3.spot, b->pc4.spot, b->pc5.spot, b->pc6.spot, b->pc7.spot, b->pc8.spot, b->pc9.spot, b->pc10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pd1.spot, b->pd2.spot, b->pd3.spot, b->pd4.spot, b->pd5.spot, b->pd6.spot, b->pd7.spot, b->pd8.spot, b->pd9.spot, b->pd10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pe1.spot, b->pe2.spot, b->pe3.spot, b->pe4.spot, b->pe5.spot, b->pe6.spot, b->pe7.spot, b->pe8.spot, b->pe9.spot, b->pe10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pf1.spot, b->pf2.spot, b->pf3.spot, b->pf4.spot, b->pf5.spot, b->pf6.spot, b->pf7.spot, b->pf8.spot, b->pf9.spot, b->pf10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pg1.spot, b->pg2.spot, b->pg3.spot, b->pg4.spot, b->pg5.spot, b->pg6.spot, b->pg7.spot, b->pg8.spot, b->pg9.spot, b->pg10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->ph1.spot, b->ph2.spot, b->ph3.spot, b->ph4.spot, b->ph5.spot, b->ph6.spot, b->ph7.spot, b->ph8.spot, b->ph9.spot, b->ph10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pi1.spot, b->pi2.spot, b->pi3.spot, b->pi4.spot, b->pi5.spot, b->pi6.spot, b->pi7.spot, b->pi8.spot, b->pi9.spot, b->pi10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n\n", b->pj1.spot, b->pj2.spot, b->pj3.spot, b->pj4.spot, b->pj5.spot, b->pj6.spot, b->pj7.spot, b->pj8.spot, b->pj9.spot, b->pj10.spot);

}

static void print_start (Board * b) {


  printf("\n%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->a1.spot, b->a2.spot, b->a3.spot, b->a4.spot, b->a5.spot, b->a6.spot, b->a7.spot, b->a8.spot, b->a9.spot, b->a10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->b1.spot, b->b2.spot, b->b3.spot, b->b4.spot, b->b5.spot, b->b6.spot, b->b7.spot, b->b8.spot, b->b9.spot, b->b10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->c1.spot, b->c2.spot, b->c3.spot, b->c4.spot, b->c5.spot, b->c6.spot, b->c7.spot, b->c8.spot, b->c9.spot, b->c10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->d1.spot, b->d2.spot, b->d3.spot, b->d4.spot, b->d5.spot, b->d6.spot, b->d7.spot, b->d8.spot, b->d9.spot, b->d10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->e1.spot, b->e2.spot, b->e3.spot, b->e4.spot, b->e5.spot, b->e6.spot, b->e7.spot, b->e8.spot, b->e9.spot, b->e10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->f1.spot, b->f2.spot, b->f3.spot, b->f4.spot, b->f5.spot, b->f6.spot, b->f7.spot, b->f8.spot, b->f9.spot, b->f10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->g1.spot, b->g2.spot, b->g3.spot, b->g4.spot, b->g5.spot, b->g6.spot, b->g7.spot, b->g8.spot, b->g9.spot, b->g10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->h1.spot, b->h2.spot, b->h3.spot, b->h4.spot, b->h5.spot, b->h6.spot, b->h7.spot, b->h8.spot, b->h9.spot, b->h10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->i1.spot, b->i2.spot, b->i3.spot, b->i4.spot, b->i5.spot, b->i6.spot, b->i7.spot, b->i8.spot, b->i9.spot, b->i10.spot);
  printf("------------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->j1.spot, b->j2.spot, b->j3.spot, b->j4.spot, b->j5.spot, b->j6.spot, b->j7.spot, b->j8.spot, b->j9.spot, b->j10.spot);

}

static void print_start2(Board * b) {

  printf("\n%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pa1.spot, b->pa2.spot, b->pa3.spot, b->pa4.spot, b->pa5.spot, b->pa6.spot, b->pa7.spot, b->pa8.spot, b->pa9.spot, b->pa10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pb1.spot, b->pb2.spot, b->pb3.spot, b->pb4.spot, b->pb5.spot, b->pb6.spot, b->pb7.spot, b->pb8.spot, b->pb9.spot, b->pb10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pc1.spot, b->pc2.spot, b->pc3.spot, b->pc4.spot, b->pc5.spot, b->pc6.spot, b->pc7.spot, b->pc8.spot, b->pc9.spot, b->pc10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pd1.spot, b->pd2.spot, b->pd3.spot, b->pd4.spot, b->pd5.spot, b->pd6.spot, b->pd7.spot, b->pd8.spot, b->pd9.spot, b->pd10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pe1.spot, b->pe2.spot, b->pe3.spot, b->pe4.spot, b->pe5.spot, b->pe6.spot, b->pe7.spot, b->pe8.spot, b->pe9.spot, b->pe10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pf1.spot, b->pf2.spot, b->pf3.spot, b->pf4.spot, b->pf5.spot, b->pf6.spot, b->pf7.spot, b->pf8.spot, b->pf9.spot, b->pf10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pg1.spot, b->pg2.spot, b->pg3.spot, b->pg4.spot, b->pg5.spot, b->pg6.spot, b->pg7.spot, b->pg8.spot, b->pg9.spot, b->pg10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->ph1.spot, b->ph2.spot, b->ph3.spot, b->ph4.spot, b->ph5.spot, b->ph6.spot, b->ph7.spot, b->ph8.spot, b->ph9.spot, b->ph10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n", b->pi1.spot, b->pi2.spot, b->pi3.spot, b->pi4.spot, b->pi5.spot, b->pi6.spot, b->pi7.spot, b->pi8.spot, b->pi9.spot, b->pi10.spot);
  printf("-----------------------------------------------\n");
  printf("%s | %s | %s | %s | %s | %s | %s | %s | %s | %s\n\n", b->pj1.spot, b->pj2.spot, b->pj3.spot, b->pj4.spot, b->pj5.spot, b->pj6.spot, b->pj7.spot, b->pj8.spot, b->pj9.spot, b->pj10.spot);

}

static void new_board (Board * b) {

  Board c = {{"A1", 0}, {"A2", 0}, {"A3", 0}, {"A4", 0}, {"A5", 0}, {"A6", 0}, {"A7", 0}, {"A8", 0}, {"A9", 0}, {"A10", 0},
         {"B1", 0}, {"B2", 0}, {"B3", 0}, {"B4", 0}, {"B5", 0}, {"B6", 0}, {"B7", 0}, {"B8", 0}, {"B9", 0}, {"B10", 0},
         {"C1", 0}, {"C2", 0}, {"C3", 0}, {"C4", 0}, {"C5", 0}, {"C6", 0}, {"C7", 0}, {"C8", 0}, {"C9", 0}, {"C10", 0},
         {"D1", 0}, {"D2", 0}, {"D3", 0}, {"D4", 0}, {"D5", 0}, {"D6", 0}, {"D7", 0}, {"D8", 0}, {"D9", 0}, {"D10", 0},
         {"E1", 0}, {"E2", 0}, {"E3", 0}, {"E4", 0}, {"E5", 0}, {"E6", 0}, {"E7", 0}, {"E8", 0}, {"E9", 0}, {"E10", 0},
         {"F1", 0}, {"F2", 0}, {"F3", 0}, {"F4", 0}, {"F5", 0}, {"F6", 0}, {"F7", 0}, {"F8", 0}, {"F9", 0}, {"F10", 0},
         {"G1", 0}, {"G2", 0}, {"G3", 0}, {"G4", 0}, {"G5", 0}, {"G6", 0}, {"G7", 0}, {"G8", 0}, {"G9", 0}, {"G10", 0},
         {"H1", 0}, {"H2", 0}, {"H3", 0}, {"H4", 0}, {"H5", 0}, {"H6", 0}, {"H7", 0}, {"H8", 0}, {"H9", 0}, {"H10", 0},
         {"I1", 0}, {"I2", 0}, {"I3", 0}, {"I4", 0}, {"I5", 0}, {"I6", 0}, {"I7", 0}, {"I8", 0}, {"I9", 0}, {"I10", 0},
         {"J1", 0}, {"J2", 0}, {"J3", 0}, {"J4", 0}, {"J5", 0}, {"J6", 0}, {"J7", 0}, {"J8", 0}, {"J9", 0}, {"J10", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0}, {"  ", 0},
         2, 3, 4, 5, 6};

  *b = c;

}

int battle () {

  int players, check, stop = 0, size, m1 = 0, m2 = 0, game_over = 0, hit1, hit2, turn;
  int mode, check2, dir, control, difficulty, loop;
  int tan[20];
  char player1[MAX + 1];
  char player2[MAX + 1];
  char player3[8] = "Eleicia";
  char playert[13] = "Sean Connery";
  char tam[20];
  char move, move2;
  char trash;
  Board b1, b2;

  system("clear");

  printf("\nWelcome to BattleShips!\n\nHow many players, 1 or 2?\nUser: ");
  scanf("%d", &players);

  if (players == 1) {

    printf("\nPlease enter your name player 1: ");
    scanf("%s", player1);
    printf("\nWhat mode would you like to play?\n   1. Regular BattleShip\n   2. Time Attack(The Hunt for Red October)\nUser: ");
    scanf("%d", &mode);

    if (mode == 1) {

      printf("What difficulty would you like?\n   1. Easy\n   2. Medium\nUser: ");
      scanf("%d", &difficulty);

      if (difficulty != 1 && difficulty != 2) {

    printf("Numbers are hard!\n^That was you^\nI am just so sorry we don't have a difficulty just for your...level\n");
    return 0;
      }

      printf("%s will be your opponent. Good Luck to you both!\n", player3);
      printf("\n%s: Don't worry %s, I will go easy on you.", player3, player1);
      sleep(2);
      system("clear");

      while(!stop) {

    control = 0;    
    check = 1;
    check2 = 1;
    hit1 = 0;
    hit2 = 0;
    turn = 0;
    new_board(&b1);
    new_board(&b2);
    
    printf("\n%s it is now time for you to place your ships(Correct format: A2 - A3, or A6 - C6.  with spaces, and no diagonal ships.)\n", player1);
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your destroyer(Destroyers are 2 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 2;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your cruiser(Cruisers are 3 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 3;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
        print_start2(&b1);
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your submarine(Submarines are 4 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 4;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
        print_start2(&b1);
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your battleship(BattleShips are 5 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 5;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
        print_start2(&b1);
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your aircraft carrier(Aircraft Carriers are 6 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 6;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
        print_start2(&b1);
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    printf("\n%s it is now time for you to place your ships(Correct format: A2 - A3, or A6 - C6.  with spaces, and no diagonal ships.)\n", player3);
    
    while (check) {
    
      size = 2;

      while (check2) {

        move = (char)(random(10) + 64);
        m1 = random(10);
        dir = random(4);

        if (dir == 1) {

          move2 = (char)((int)move - (size - 1));
          m2 = m1;
        } else if (dir == 2) {

          move2 = move;
          m2 = m1 + (size - 1);          
        } else if (dir == 3) {
          
          move2 = (char)((int)move + (size - 1));
          m2 = m1;
        } else {
          
          move2 = move;
          m2 = m1 - (size - 1);
        }
        
        check = check_placement(&b2, move, move2, m1, m2, size);
      
        if (!check) {
          
          check2 = 0;
          place_piece(&b2, move, move2, m1, m2,  size);
        }
      }
    }
    check = 1;
    check2 = 1;

    while (check) {
    
      size = 3;

      while (check2) {

        move = (char)(random(10) + 64);
        m1 = random(10);
        dir = random(4);

        if (dir == 1) {

          move2 = (char)((int)move - (size - 1));
          m2 = m1;
        } else if (dir == 2) {

          move2 = move;
          m2 = m1 + (size - 1);          
        } else if (dir == 3) {
          
          move2 = (char)((int)move + (size - 1));
          m2 = m1;
        } else {
          
          move2 = move;
          m2 = m1 - (size - 1);
        }
        
        check = check_placement(&b2, move, move2, m1, m2, size);
      
        if (!check) {
          
          check2 = 0;
          place_piece(&b2, move, move2, m1, m2,  size);
        }
      }
    }
    check = 1;
    check2 = 1;

    while (check) {
    
      size = 4;

      while (check2) {

        move = (char)(random(10) + 64);
        m1 = random(10);
        dir = random(4);

        if (dir == 1) {

          move2 = (char)((int)move - (size - 1));
          m2 = m1;
        } else if (dir == 2) {

          move2 = move;
          m2 = m1 + (size - 1);          
        } else if (dir == 3) {
          
          move2 = (char)((int)move + (size - 1));
          m2 = m1;
        } else {
          
          move2 = move;
          m2 = m1 - (size - 1);
        }
        
        check = check_placement(&b2, move, move2, m1, m2, size);
      
        if (!check) {
          
          check2 = 0;
          place_piece(&b2, move, move2, m1, m2,  size);
        }
      }
    }
    check = 1;
    check2 = 1;

    while (check) {
    
      size = 5;

      while (check2) {

        move = (char)(random(10) + 64);
        m1 = random(10);
        dir = random(4);

        if (dir == 1) {

          move2 = (char)((int)move - (size - 1));
          m2 = m1;
        } else if (dir == 2) {

          move2 = move;
          m2 = m1 + (size - 1);          
        } else if (dir == 3) {
          
          move2 = (char)((int)move + (size - 1));
          m2 = m1;
        } else {
          
          move2 = move;
          m2 = m1 - (size - 1);
        }
        
        check = check_placement(&b2, move, move2, m1, m2, size);
      
        if (!check) {
          
          check2 = 0;
          place_piece(&b2, move, move2, m1, m2,  size);
        }
      }
    }
    check = 1;
    check2 = 1;

    while (check) {
    
      size = 6;

      while (check2) {

        move = (char)(random(10) + 64);
        m1 = random(10);
        dir = random(4);

        if (dir == 1) {

          move2 = (char)((int)move - (size - 1));
          m2 = m1;
        } else if (dir == 2) {

          move2 = move;
          m2 = m1 + (size - 1);          
        } else if (dir == 3) {
          
          move2 = (char)((int)move + (size - 1));
          m2 = m1;
        } else {
          
          move2 = move;
          m2 = m1 - (size - 1);
        }
        
        check = check_placement(&b2, move, move2, m1, m2, size);
      
        if (!check) {
          
          check2 = 0;
          place_piece(&b2, move, move2, m1, m2,  size);
        }
      }
    }
    check = 1;
    check2 = 1;
      
    while(!game_over) {

      turn++;
      system("clear");
      
      scanf("%c", &trash);
      printf("It is %s's turn. %s press enter to continue.", player1, player1);
      scanf("%c", &trash);
      
      if (hit1) {
        
        printf("Admiral, we suffered a direct hit! Please sir, save us!\n");
      } else if (turn != 1) {
        
        printf("Admiral, that sissy hatter must be shooting with their eyes closed! a miss!\n");
      }
      hit1 = 0;
      
      while (check) {
        
        print_board(&b1);
        printf("Please enter coordinates to attack, admiral %s!\nCoordinates: ", player1);
        scanf(" %c%d", &move, &m1);
        
        check = check_move(&b1, &b2, move, m1);
        
        if (check == 0) {
          
          printf("PREPARING TO FIRE!!!\n\n");
          sleep(2);
          system("clear");
          
          if (fire(&b1, &b2, move, m1)) {
        
        print_board(&b1);
        printf("Admiral a direct hit!\n");
          } else {
        
        print_board(&b1);
        printf("Admiral we have missed!\n");
          }
          
        } else if (check == 1) {
          
          printf("Admiral, we gain nothing from attacking positions we have already cleared!\n");
          sleep(1);
          system("clear");
        } else {
          
          printf("Please, %s, focus. We have a war to win!\n", player1);
          sleep(1);
          system("clear");
        }
      }
      check = 1;
      
      game_over = check_victory(&b2);
      sleep(2);

      if (!game_over) {
        
        system("clear");

        scanf("%c", &trash);
        printf("It is %s's turn.", player3);
        
        
        if (difficulty == 1) {
          
          while (check) {
        
        move = (char)(random(10) + 64);
        m1 = random(10);
        
        check = check_move(&b2, &b1, move, m1);
        
        if (check == 0) {
          if (fire(&b2, &b1, move, m1)) {
            
            hit1 = 1;
          }
        }
          }
        } else {
          
          while (check) {
        
        if (hit2 == 0) {

          move = (char)(random(10) + 64);
          m1 = random(10);
          check = check_move(&b2, &b1, move, m1);
        } else {

          dir = random(4);

          if (dir == 1) {

            move = (char)((int)move2 - 1);
            m1 = m2;
            check = check_move(&b2, &b1, move, m1);
            control++;
          } else if (dir == 2) {

            move = move2;
            m1 = m2 + 1;
            check = check_move(&b2, &b1, move, m1);
            control++;
          } else if (dir == 3) {

            move = (char)((int)move2 + 1);
            m1 = m2;
            check = check_move(&b2, &b1, move, m1);
            control++;
          } else {

            move = move2;
            m1 = m2 - 1;
            check = check_move(&b2, &b1, move, m1);
            control++;
          }
        }

        if (control == 50000) {

          hit2 = 0;
          control = 0;
        }

        if (check == 0) {
          if (fire(&b2, &b1, move, m1)) {
            
            hit1 = 1;
            hit2 = 1;
            move2 = move;
            m2 = m1;
          }
        }
          }
        }
      check = 1;
      control = 0;

      sleep(2);
      game_over = check_victory_comp(&b1);
      
      if (game_over == 2) {

        hit2 = 0;
        game_over = 0;
      }          
     
      if (game_over) {

        system("clear");
        printf("\n%s has successfully routed %s's forces! The day is ours!\n", player2, player1);
        print_board(&b2);
      }
      
      sleep(2);
      } else {
        
        system("clear");
        printf("\n%s has successfully sunken all of %s's ships! Victory is ours!\n", player1, player2);
        print_board(&b1);
      }
    }
    
    printf("\nDo you want to play again?\n   1. Quick Replay\n   2. New Game\n   3. No\nUser: ");
    scanf("%d", &stop);
    
    if (stop == 1) {
       
      stop = 0;
      game_over = 0;
      system("clear");
    } else if (stop == 2) {
      
      stop = 0;
      system("clear");
      battle();
    } else {

      stop = 1;
    }
      }/*to stop playing*/
    } else if (mode == 2) {
      
      system("clear");

      printf("In this mode you have to locate and destroy the enemies submarine before your fleet is destroyed.\n");      
      printf("Every 5 turns you get an artillery strike on the enemy.\nBut, be wary, your intel was leaked. The enemy knows your every move!\n");
      printf("The commander of the enemy sub is %s.\nI wish you both luck\n", playert);

      sleep(10);

      system("clear");    
      while(!stop) {

    sizet = 0;
    control = 0;    
    check = 1;
    check2 = 1;
    hit1 = 0;
    hit2 = 0;
    turn = 0;
    new_board(&b1);
    new_board(&b2);
    
    printf("\n%s it is now time for you to place your ships(Correct format: A2 - A3, or A6 - C6.  with spaces, and no diagonal ships.)\n", player1);
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your destroyer(Destroyers are 2 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 2;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
        extract_t(move, move2, m1, m2, tam, tan);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your cruiser(Cruisers are 3 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 3;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
        print_start2(&b1);
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
        extract_t(move, move2, m1, m2, tam, tan);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your submarine(Submarines are 4 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 4;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
        print_start2(&b1);
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
        extract_t(move, move2, m1, m2, tam, tan);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your battleship(BattleShips are 5 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 5;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
        print_start2(&b1);
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
        extract_t(move, move2, m1, m2, tam, tan);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    while (check) {
      
      print_start(&b1);
      printf("\nchoose a spot for your aircraft carrier(Aircraft Carriers are 6 squares): ");
      scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
      size = 6;
      check = check_placement(&b1, move, move2, m1, m2, size);
      
      if (check) {
        
        printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);
        
        printf("Error! Invalid Move!\n");
        print_start2(&b1);
      } else {
        
        place_piece(&b1, move, move2, m1, m2,  size);
        extract_t(move, move2, m1, m2, tam, tan);
      }
    }
    check = 1;
    print_start2(&b1);
    
    sleep(2);
    system("clear");
    
    printf("\n%s it is time for you to hide your sub.\n", playert);

    while (check) {
    
      size = 4;

      while (check2) {

        move = (char)(random(10) + 64);
        m1 = random(10);
        dir = random(4);

        if (dir == 1) {

          move2 = (char)((int)move - (size - 1));
          m2 = m1;
        } else if (dir == 2) {

          move2 = move;
          m2 = m1 + (size - 1);          
        } else if (dir == 3) {
          
          move2 = (char)((int)move + (size - 1));
          m2 = m1;
        } else {
          
          move2 = move;
          m2 = m1 - (size - 1);
        }
        
        check = check_placement(&b2, move, move2, m1, m2, size);
      
        if (!check) {
          
          check2 = 0;
          place_piece(&b2, move, move2, m1, m2,  size);
        }
      }
    }
    check = 1;
    check2 = 1;
      
    while(!game_over) {

      turn++;
      system("clear");
      
      scanf("%c", &trash);
      printf("It is %s's turn. %s press enter to continue.", player1, player1);
      scanf("%c", &trash);
      
      if (hit1) {
        
        printf("Admiral, we suffered a direct hit! Please sir, save us!\n");
      } else if (turn != 1) {
        
        printf("Admiral, that sissy hatter must be shooting with their eyes closed! a miss!\n");
      }
      hit1 = 0;

      if (turn % 5 == 0) {

        printf("Sir, the artillery is ready to fire!\n");
    
        sleep(1);
        printf("FIRE!!!");

        for(loop = 0; loop < ARTILLERY; loop++) {

          while (check) {

        move = (char)(random(10) + 64);
        m1 = random(10);
        check = check_move(&b1, &b2, move, m1);
          }
          check = 1;
          if (fire(&b1, &b2, move, m1)) {

        printf("Hit!");
          } else {

        printf("Miss!");
          }
        }
      }
      check = 1;

      game_over = check_victory_t(&b2);

      if (!game_over) {

        while (check) {
        
          print_board(&b1);
          printf("Please enter coordinates to attack, admiral %s!\nCoordinates: ", player1);
          scanf(" %c%d", &move, &m1);
          
          check = check_move(&b1, &b2, move, m1);
          
          if (check == 0) {
        
        printf("PREPARING TO FIRE!!!\n\n");
        sleep(2);
        system("clear");
          
        if (fire(&b1, &b2, move, m1)) {
          
          print_board(&b1);
          printf("Admiral a direct hit!\n");
        } else {
          
          print_board(&b1);
          printf("Admiral we have missed!\n");
        }
        
          } else if (check == 1) {
        
        printf("Admiral, we gain nothing from attacking positions we have already cleared!\n");
        sleep(1);
        system("clear");
          } else {
        
        printf("Please, %s, focus. We have a war to win!\n", player1);
        sleep(1);
        system("clear");
          }
        }
        check = 1;
        
        game_over = check_victory_t(&b2);
        sleep(2);
        
        if (!game_over) {
          
          system("clear");
          
          while (check) {
          
        move = tam[turn - 1];
        m1 = tan[turn - 1];

        check = check_move(&b2, &b1, move, m1);

        if (check == 0) {
          if (fire(&b2, &b1, move, m1)) {
            
            hit1 = 1;
          }
        }
          }
          
          check = 1;
          control = 0;
          
          sleep(2);
          game_over = check_victory(&b1);
          
          if (game_over) {
        
        system("clear");
        printf("\n%s has successfully routed %s's forces! The day is his!\n", playert, player1);
        print_board(&b2);
          }
          
          sleep(2);
        } else {
          
          system("clear");
          printf("\n%s has successfully found %s and destroyed the Red October! Victory is ours!\n", player1, playert);
          print_board(&b1);
        }
      } else {
        
        system("clear");
        printf("\n%s, that was close. The artillery was a good investment!\n", player1);
        print_board(&b1);
      }
    }
    
    printf("\nDo you want to play again?\n   1. Quick Replay\n   2. New Game\n   3. No\nUser: ");
    scanf("%d", &stop);
    
    if (stop == 1) {
      
      stop = 0;
      game_over = 0;
      system("clear");
    } else if (stop == 2) {
      
      stop = 0;
      system("clear");
      battle();
    } else {

      stop = 1;
    }
      }/*to stop playing*/
    } else {
      
      printf("Numbers are not hard\n");
      return 0;
    }
  } else if (players == 2) {
    
    printf("\nPlease enter your name player 1: ");
    scanf("%s", player1);
    printf("\nPlease enter your name player 2: ");
    scanf("%s", player2);
    
    while(!stop) {
      
      check = 1;
      hit1 = 0;
      hit2 = 0;
      turn = 0;
      new_board(&b1);
      new_board(&b2);

      printf("\n%s it is now time for you to place your ships(Correct format: A2 - A3, or A6 - C6.  with spaces, and no diagonal ships.)\n", player1);

      while (check) {

    print_start(&b1);
    printf("\nchoose a spot for your destroyer(Destroyers are 2 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 2;
    check = check_placement(&b1, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
    } else {

      place_piece(&b1, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b1);

      sleep(2);
      system("clear");

      while (check) {

    print_start(&b1);
    printf("\nchoose a spot for your cruiser(Cruisers are 3 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 3;
    check = check_placement(&b1, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
      print_start2(&b1);
    } else {

      place_piece(&b1, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b1);

      sleep(2);
      system("clear");

      while (check) {

    print_start(&b1);
    printf("\nchoose a spot for your submarine(Submarines are 4 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 4;
    check = check_placement(&b1, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
      print_start2(&b1);
    } else {

      place_piece(&b1, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b1);

      sleep(2);
      system("clear");

      while (check) {

    print_start(&b1);
    printf("\nchoose a spot for your battleship(BattleShips are 5 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 5;
    check = check_placement(&b1, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
      print_start2(&b1);
    } else {

      place_piece(&b1, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b1);

      sleep(2);
      system("clear");

      while (check) {

    print_start(&b1);
    printf("\nchoose a spot for your aircraft carrier(Aircraft Carriers are 6 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 6;
    check = check_placement(&b1, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
      print_start2(&b1);
    } else {

      place_piece(&b1, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b1);

      sleep(2);
      system("clear");

      printf("\n%s it is now time for you to place your ships(Correct format: A2 - A3, or A6 - C6.  with spaces, and no diagonal ships.)\n", player2);

      sleep(2);

      while (check) {

    print_start(&b2);
    printf("\nchoose a spot for your destroyer(Destroyers are 2 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 2;
    check = check_placement(&b2, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
    } else {

      place_piece(&b2, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b2);

      sleep(2);
      system("clear");

      while (check) {

    print_start(&b2);
    printf("\nchoose a spot for your cruiser(Cruisers are 3 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 3;
    check = check_placement(&b2, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
      print_start2(&b2);
    } else {

      place_piece(&b2, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b2);

      sleep(2);
      system("clear");

      while (check) {

    print_start(&b2);
    printf("\nchoose a spot for your submarine(Submarines are 4 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 4;
    check = check_placement(&b2, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
      print_start2(&b2);
    } else {

      place_piece(&b2, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b2);

      sleep(2);
      system("clear");

      while (check) {

    print_start(&b2);
    printf("\nchoose a spot for your battleship(BattleShips are 5 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 5;
    check = check_placement(&b2, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
      print_start2(&b2);
    } else {

      place_piece(&b2, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b2);

      sleep(2);
      system("clear");

      while (check) {

    print_start(&b2);
    printf("\nchoose a spot for your aircraft carrier(Aircraft Carriers are 6 squares): ");
    scanf(" %c%d %c %c%d", &move, &m1, &trash, &move2, &m2);
    size = 6;
    check = check_placement(&b2, move, move2, m1, m2, size);

    if (check) {

      printf("\n%c%d : %c%d\n\n", move, m1, move2, m2);

      printf("Error! Invalid Move!\n");
      print_start2(&b2);
    } else {

      place_piece(&b2, move, move2, m1, m2,  size);
    }
      }
      check = 1;
      print_start2(&b2);

      sleep(2);
      system("clear");

      while(!game_over) {

    turn++;
    system("clear");

    scanf("%c", &trash);
    printf("It is %s's turn. %s press enter to continue.", player1, player1);
    scanf("%c", &trash);

    if (hit1) {

      printf("Admiral, we suffered a direct hit! Please sir, save us!\n");
    } else if (turn != 1) {

        printf("Admiral, that sissy hatter must be shooting with their eyes closed! a miss!\n");
    }
    hit1 = 0;

    while (check) {

      print_board(&b1);
      printf("Please enter coordinates to attack, admiral %s!\nCoordinates: ", player1);
      scanf(" %c%d", &move, &m1);

      check = check_move(&b1, &b2, move, m1);

      if (check == 0) {

        printf("PREPARING TO FIRE!!!\n\n");
        sleep(2);
        system("clear");

        if (fire(&b1, &b2, move, m1)) {

          print_board(&b1);
          printf("Admiral a direct hit!\n");
          hit2 = 1;
        } else {

          print_board(&b1);
          printf("Admiral we have missed!\n");
        }

      } else if (check == 1) {

        printf("Admiral, we gain nothing from attacking positions we have already cleared!\n");
        sleep(1);
        system("clear");
      } else {

        printf("Please, %s, focus. We have a war to win!\n", player1);
        sleep(1);
        system("clear");
      }
    }
    check = 1;

    sleep(2);
    game_over = check_victory(&b2);
    sleep(2);

    if (!game_over) {

      system("clear");

      scanf("%c", &trash);
      printf("It is %s's turn. %s press enter to continue.", player2, player2);
      scanf("%c", &trash);

      system("clear");

      if (hit2) {

        printf("Commander, a direct hit! Perhaps we have an intel leak!\n");
      } else if (turn != 1) {
        
        printf("Commander, they might as well be throwing stones at us! a miss!\n");
      }
      hit2 = 0;
      
      while (check) {
        
        print_board(&b2);
        printf("Please enter coordinates to attack, commander %s!\nCoordinates: ", player2);
        scanf(" %c%d", &move, &m1);
        
        check = check_move(&b2, &b1, move, m1);
        
        if (check == 0) {
          
          printf("PREPARING TO FIRE!!!\n\n");
          sleep(2);
          system("clear");
          
          if (fire(&b2, &b1, move, m1)) {
        
        print_board(&b2);
        printf("Commander a direct hit!\n");
        hit1 = 1;
          } else {
        
        print_board(&b2);
        printf("Commander we have missed!\n");
          }
          
        } else if (check == 1) {
          
          printf("Commander, we gain nothing from attacking positions we have already cleared!\n");
          sleep(1);
          system("clear");
        } else {
          
          printf("Please, %s, focus. We have an enemy to defeat!\n", player2);
          sleep(1);
          system("clear");
        }
      }
      check = 1;
      

      sleep(2);
      game_over = check_victory(&b1);

      if (game_over) {
        system("clear");
        printf("\n%s has successfully routed %s's forces! The day is ours!\n", player2, player1);
        print_board(&b2);
      }

      sleep(2);
    } else {

      system("clear");
      printf("\n%s has successfully sunken all of %s's ships! Victory is ours!\n", player1, player2);
      print_board(&b1);
    }
      }

      printf("\nDo you want to play again?\n   1. Quick Replay\n   2. New Game\n   3. No\nUser: ");
      scanf("%d", &stop);
      
      if (stop == 1) {


    stop = 0;
    game_over = 0;
    system("clear");
      } else if (stop == 2) {

    stop = 0;
    system("clear");
    battle();
      }
      stop = 1;
    } /*the while loop to keep playing*/
  } else {

    printf("Fine! I guess you hate murica!\n");
    return 0;
  }

  return 0;
}